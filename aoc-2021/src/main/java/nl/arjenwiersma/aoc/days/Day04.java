package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.arjenwiersma.aoc.common.Day;

public class Day04 implements Day<Integer> {
    public int playGame(List<String> input, int index) {
        List<int[][]> boards = new ArrayList<>();

        int[] nums = Arrays.stream(input.get(0).split(",")).mapToInt(Integer::parseInt).toArray();

        for (int i = 1; i < input.size(); i += 6) {
            int[][] b = new int[5][5];

            for (int rij = 1; rij <= 5; rij++) {
                int kolom = 0;
                for (int v : Arrays.stream(input.get(i + rij).trim().split("\\s+")).mapToInt(Integer::parseInt).toArray()) {
                    b[rij-1][kolom] = v;
                    kolom += 1;
                }
            }
            boards.add(b);
        }

        List<Integer> oplossingen = new ArrayList<>();
        Map<Integer, Integer> eindwaardes = new HashMap<>();
        for (int n : nums) {
            for (int bn = 0; bn < boards.size(); bn++) {
                int[][] b = boards.get(bn);
                for (int rij = 0; rij < 5; rij++) {
                    for (int kolom=0; kolom < 5; kolom++) {
                        if (!oplossingen.contains(bn)) {
                            if (b[rij][kolom] == n) b[rij][kolom] = -1; // mark it as BINGO-ed

                            if (b[rij][0] + b[rij][1] + b[rij][2] + b[rij][3] + b[rij][4] == -5 ||
                                    b[0][kolom] + b[1][kolom] + b[2][kolom] + b[3][kolom] + b[4][kolom] == -5) {
                                int waarde = Arrays.stream(b).flatMapToInt(Arrays::stream).filter(z -> z != -1).sum();
                                oplossingen.add(bn); // Add the bingo card at the time it hash BINGO
                                eindwaardes.put(bn, waarde*n);
                            }
                        }
                    }
                }
            }
        }

        return eindwaardes.get(oplossingen.get(index == -1 ? oplossingen.size() - 1 : index));
    }


    @Override
    public Integer part1(List<String> input) {
        return playGame(input, 0);
    }

    @Override
    public Integer part2(List<String> input) {
        return playGame(input, -1);
    }
}

