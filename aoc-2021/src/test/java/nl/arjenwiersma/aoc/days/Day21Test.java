package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day21Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(21);

    Day21 day;
    
    @Before
    public void before() {
        day = new Day21();
    }

    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {{
                add("Player 1 starting position: 4");
                add("Player 2 starting position: 8");
            }};

        assertEquals(739785L, (long)day.part1(input));
        assertEquals(444356092776315L, (long) day.part2(input));
    }

    @Test
    public void part1() {
        assertEquals(888735L, (long)day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(647608359455719L, (long)day.part2(input.getLines()));
    }
}
