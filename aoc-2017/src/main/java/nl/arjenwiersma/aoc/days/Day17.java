package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.List;

import nl.arjenwiersma.aoc.common.Day;

/**
 * Day17
 */
public class Day17 extends Day<Integer, Integer> {
    @Override
    public Integer parseInput(List<String> input) {
        return Integer.parseInt(input.get(0));
    }

    @Override
    public Integer solver1(Integer input) {
        List<Integer> vortex = new ArrayList<>(2018);
        vortex.add(0, 0);
        int pos = 0;
        for (int index = 1; index <= 2017; index++) {
            pos = ((pos + input) % index) +1;  // note vortex.size() == index
            vortex.add(pos, index);
        }
        return vortex.get(pos + 1);
    }

    @Override
    public Integer solver2(Integer input) {
        /**
         * To know the value after 0 we only need to track the iterations
         * that actually land at 1, as 0 will always be the first element in
         * list, the update to the 1st element is what we care about
         */
        int pos = 0;
        int value = 0;
        for (int index = 1; index <= 50000000; index++) {
            pos = ((pos + input) % index)  + 1; 
            if (pos == 1)
                value = index;
        }
        return value;

    }
}
