package nl.arjenwiersma.aoc.common;

import java.util.List;

public abstract class NewDay<I,R> {

    public R part1(List<String> input) {
        I parsed = parseInput(input);
        return solver1(parsed);
    }

    public R part2(List<String> input) {
        I parsed = parseInput(input);
        return solver2(parsed);
    }

    public abstract I parseInput(List<String> input);
    public abstract R solver1(I input);
    public abstract R solver2(I input);
}
