package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import nl.arjenwiersma.aoc.common.NewDay;

public class Day07 extends NewDay<Day07.Node, Long> {

    public class Node {
        int type;
        String name;
        long size;
        Node parent;
        List<Node> children;

        public Node(int type, String name, long size, Node parent, List<Node> children) {
            this.type = type;
            this.name = name;
            this.size = size;
            this.parent = parent;
            this.children = children;
        }

        public long getSize() {
            if (size > 0) {
                // System.out.println(name + " " + size);
                return size;
            }

            long acc = 0;
            for (Node n : children) {
                acc += n.getSize();
            }
            // System.out.println(name + " total size: " + acc);

            return acc;
        }

        public List<Node> gather(long atmost) {
            List<Node> matches = new ArrayList<>();

            if (type == 0 && getSize() <= atmost) {
                matches.add(this);
            }

            if (children != null) {
                for (Node n : children) {
                    matches.addAll(n.gather(atmost));
                }
            }

            return matches;
        }

        public List<Node> find(long minimum) {
            List<Node> matches = new ArrayList<>();

            if (type == 0 && getSize() >= minimum) {
                matches.add(this);
            }

            if (children != null) {
                for (Node n : children) {
                    matches.addAll(n.find(minimum));
                }
            }

            return matches;
        }

        @Override
        public String toString() {
            return "Node [children=" + children + ", name=" + name + ", parent="
                    + (parent != null ? parent.name : "no parent") + ", size=" + size + ", type=" + type + "]";
        }

    }

    @Override
    public Day07.Node parseInput(List<String> input) {
        Node root = new Node(0, "/", 0, null, new ArrayList<>());
        Node current = root;
        for (String in : input) {
            String[] parts = in.split(" ");
            switch (parts[0]) {
            case "$" -> {
                switch (parts[1]) {
                case "cd" -> {
                    if ("/".equals(parts[2])) {
                        current = root;
                    } else if ("..".equals(parts[2])) {
                        current = current.parent;
                    } else {
                        Node change = new Node(0, parts[2], 0, current, new ArrayList<>());
                        current.children.add(change);
                        current = change;
                    }
                }
                case "ls" -> {
                }
                default -> {
                }
                }
            }
            default -> {
                if (!"dir".equals(parts[0])) {
                    Node file = new Node(1, parts[1], (long) Long.parseLong(parts[0]), current, null);
                    current.children.add(file);
                }
            }
            }
        }
        return root;
    }

    @Override
    public Long solver1(Node input) {
        List<Node> gather = input.gather(100000);

        long answer = 0;
        for (Node n : gather) {
            answer += n.getSize();
        }
        return answer;
    }

    @Override
    public Long solver2(Node input) {
        long total = 70000000;
        long needed = 30000000;
        long inuse = input.getSize();
        long find = needed - (total - inuse);

        System.out.println("Finding " + find + " bytes");
        List<Node> gather = input.find(find);

        return gather.stream().mapToLong(x -> x.getSize()).min().getAsLong();
    }

}
