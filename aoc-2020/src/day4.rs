use std::collections::HashMap;

use regex::Regex;

#[derive(Debug)]
pub struct Passport(HashMap<String, String>);

#[aoc_generator(day4)]
pub fn input_generator(input: &str) -> Vec<Passport> {
    input
        .split("\n\n")
        .map(|l| {
            let n = l.replace("\n", " ");
            let split = n.split(' ').collect::<Vec<&str>>();

            let mut h = HashMap::new();
            for s in split {
                let sp2 = s.split(':').collect::<Vec<&str>>();
                h.insert(
                    sp2.get(0).unwrap().to_string(),
                    sp2.get(1).unwrap().to_string(),
                );
            }
            Passport(h)
        })
        .collect()
}

#[aoc(day4, part1)]
pub fn part1(input: &[Passport]) -> usize {
    let mut counter = 0;

    for passport in input {
        let mut valid = true;
        for n in ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"] {
            if !passport.0.contains_key(n) {
                valid = false;
            }
        }
        if valid {
            counter += 1;
        }
    }

    counter // 230
}

#[aoc(day4, part2)]
pub fn part2(input: &[Passport]) -> usize {
    let mut v: HashMap<&str, fn(&str) -> bool> = HashMap::new();

    v.insert("byr", |s| match s.parse::<i32>() {
        Ok(x) => (1920..=2002).contains(&x),
        Err(_) => false,
    });

    v.insert("iyr", |s| match s.parse::<i32>() {
        Ok(x) => (2010..=2020).contains(&x),
        Err(_) => false,
    });

    v.insert("eyr", |s| match s.parse::<i32>() {
        Ok(x) => (2020..=2030).contains(&x),
        Err(_) => false,
    });

    v.insert("hgt", |s| {
        let re = Regex::new(r"(\d+)((?:cm|in))").unwrap();
        let cap = match re.captures(s) {
            Some(x) => x,
            None => return false,
        };

        let m = cap.get(1).map(|m| m.as_str());
        match m {
            Some(val) => {
                let l: i32 = val.parse().unwrap();
                let unit = cap.get(2).map(|m| m.as_str()).unwrap();
                if unit == "cm" {
                    (150..=193).contains(&l)
                } else if unit == "in" {
                    (59..=76).contains(&l)
                } else {
                    false
                }
            }
            None => panic!("called `Option::unwrap()` on a `None` value"),
        }
    });

    v.insert("hcl", |s| {
        let re = Regex::new(r"#[0-9a-f]{6}").unwrap();
        re.is_match(s)
    });

    v.insert("ecl", |s| {
        ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&s)
    });

    v.insert("pid", |s| {
        let re = Regex::new(r"^\d{9}$").unwrap();
        re.is_match(s)
    });

    let mut counter = 0;

    for passport in input {
        let mut valid = true;
        for n in ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"] {
            if !passport.0.contains_key(n) {
                valid = false;
            } else {
                let f = v.get(n).unwrap();
                if !f(passport.0.get(n).unwrap()) {
                    valid = false;
                }
            }
        }
        if valid {
            counter += 1;
        }
    }

    counter // 156
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = input_generator(
            "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in",
        );

        assert_eq!(part1(&input), 2);
    }

    #[test]
    fn test_part2() {
        let input = input_generator(
            "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007",
        );

        assert_eq!(part2(&input), 0);
    }

    #[test]
    fn test_part2_valid() {
        let input = input_generator(
            "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719",
        );

        assert_eq!(part2(&input), 4);
    }
}
