package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.arjenwiersma.aoc.common.NewDay;
import nl.arjenwiersma.aoc.utils.Coord;

public class Day17 extends NewDay<Coord[], Integer> {
    
    @Override
    public Coord[] parseInput(List<String> input) {
        Pattern p = Pattern.compile(".+x=(-?\\d+)..(-?\\d+), y=(-?\\d+)..(-?\\d+)");
        Matcher m = p.matcher(input.get(0));

        int x1 = 0;
        int y1 = 0;
        int x2 = 0;
        int y2 = 0;
        
        if (m.find()) {
            x1 = Integer.parseInt(m.group(1));
            x2 = Integer.parseInt(m.group(2));
            y1 = Integer.parseInt(m.group(3));
            y2 = Integer.parseInt(m.group(4));
        }

        Coord c1 = new Coord(x1, y1);
        Coord c2 = new Coord(x2, y2);

        return new Coord[] { c1, c2 };
    }


    public boolean coordPastArea(Coord current, Coord t1, Coord t2) {
        return (current.x() > t2.x() ||  current.y() < t1.y());
    }

    public boolean coordInArea(Coord current, Coord t1, Coord t2) {
        return (current.x() >= t1.x() && current.x() <= t2.x()
                && current.y() >= t1.y() && current.y() <= t2.y());
    }

    public Optional<Integer> simulatePath(int xvel, int yvel, Coord c1, Coord c2) {
        Coord mis = new Coord(0, 0);

        int maxHeight = 0;
        int i = 0;
        while (!coordPastArea(mis, c1, c2)) {
            mis = new Coord(mis.x() + xvel, mis.y() + yvel);
            maxHeight = Math.max(maxHeight, mis.y());
            
            xvel -= Integer.compare(xvel, 0);
            yvel -= 1;
            if (coordInArea(mis, c1, c2)) {
                return Optional.of(maxHeight);
            }
        }
        return Optional.empty();
    }
    
    @Override
    public Integer solver1(Coord[] input) {
        int maxHeight = Integer.MIN_VALUE;
        for (int x = 0; x < input[1].x()+1; x++) {
            for (int y = input[0].y(); y < Math.abs(input[0].y()); y++) {
                Optional<Integer> s = simulatePath(x, y, input[0], input[1]);
                if (s.isPresent()) 
                    maxHeight = Math.max(maxHeight, s.get());
            }
        }
                
        return maxHeight;
    }

    @Override
    public Integer solver2(Coord[] input) {
        List<int[]> coords = new ArrayList<>();
        
        for (int x = 0; x < input[1].x()+1; x++) {
            for (int y = input[0].y(); y < Math.abs(input[0].y()); y++) {
                Optional<Integer> s = simulatePath(x, y, input[0], input[1]);
                if (s.isPresent()) {
                    coords.add(new int[] { x, y });
                }
            }
        }
                
        return coords.size();
    }
    
}
