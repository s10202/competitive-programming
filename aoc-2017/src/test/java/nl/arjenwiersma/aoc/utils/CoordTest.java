package nl.arjenwiersma.aoc.utils;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class CoordTest {
    @Test
    public void iCanEqual() {
        Coord c1 = new Coord(1, 2);
        Coord c2 = new Coord(1, 2);

        assertEquals(c1, c2);
    }

    @Test
    public void iCreatePathBetween() {
        Coord c1 = new Coord(1, 1);
        Coord c2 = new Coord(1, 3);

        List<Coord> cs = c1.coordsBetween(c2);
        assertEquals(List.of(new Coord(1, 1), new Coord(1, 2), new Coord(1, 3)), cs);

        Coord o1 = new Coord(9, 7);
        Coord o2 = new Coord(7, 7);

        cs = o1.coordsBetween(o2);
        assertEquals(List.of(new Coord(9,7), new Coord(8, 7), new Coord(7, 7)), cs);
        
        Coord d1 = new Coord(9, 7);
        Coord d2 = new Coord(7, 9);

        cs = d1.coordsBetween(d2);
        assertEquals(List.of(new Coord(9,7), new Coord(8, 8), new Coord(7, 9)), cs);
        
    }

}
