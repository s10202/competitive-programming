package com.buildfunthings.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.buildfunthings.aoc.common.Day;
import com.buildfunthings.aoc.common.DayInputExternalResource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class Day19Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(19);

    Day<Integer> day;
    
    @Before
    public void before() {
        day = new Day19();
    }

    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {{
                add("5");
            }};
        
        assertEquals(3, (int)day.part1(input));
        assertEquals(2, (int)day.part2(input));
    }

    @Test
    public void part1() {
        assertEquals(1816277, (int)day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(1410967, (int)day.part2(input.getLines()));
    }
}
