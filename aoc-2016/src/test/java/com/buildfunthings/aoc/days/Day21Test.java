package com.buildfunthings.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.buildfunthings.aoc.common.Day;
import com.buildfunthings.aoc.common.DayInputExternalResource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class Day21Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(21);

    Day<String> day;
    
    @Before
    public void before() {
        day = new Day21();
    }

    @Test
    public void part1() {
        assertEquals("baecdfgh", day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals("cegdahbf", day.part2(input.getLines()));
    }
}
