package com.buildfunthings.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.buildfunthings.aoc.common.Day;
import com.buildfunthings.aoc.common.DayInputExternalResource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class Day20Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(20);

    Day<Long> day;
    
    @Before
    public void before() {
        day = new Day20();
    }

    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {
            {
                add("5-8");
                add("0-2");
                add("4-7");
            }
        };
        assertEquals(3L, (long)day.part1(input));
        //assertEquals(2L, (long)day.part2(input));
    }

    @Test
    public void part1() {
        assertEquals(32259706L, (long)day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(113L, (long)day.part2(input.getLines()));
    }
}
