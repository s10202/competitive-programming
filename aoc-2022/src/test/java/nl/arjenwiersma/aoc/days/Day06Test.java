package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day06Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(06);

    Day06 day;

    @Before
    public void before() {
        day = new Day06();
    }

    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {
            {
                add("mjqjpqmgbljsphdztnvjfqwrcgsmlb");
            }
        };

        assertEquals(7, (int) day.part1(input));
        assertEquals(19, (int) day.part2(input));
        assertEquals(23, (int) day.part2(new ArrayList<String>() {
            {
                add("bvwbjplbgvbhsrlpgdmjqwftvncz");
            }
        }));
    }

    @Test
    public void part1() {
        assertEquals(1655, (int) day.part1(input.getLines()));
    }

    @Test
    public void part2() {
        assertEquals(2665, (int) day.part2(input.getLines()));
    }
}
