#[aoc_generator(day3)]
pub fn input_generator(input: &str) -> Vec<Vec<bool>> {
    input
        .lines()
        .map(|l| l.chars().map(|c| '.' != c).collect())
        .collect()
}

struct Point(usize, usize);

#[aoc(day3, part1)]
pub fn part1(grid: &[Vec<bool>]) -> usize {
    let right = 3;
    let down = 1;

    count_trees(grid, right, down) // 270
}

fn count_trees(grid: &[Vec<bool>], right: usize, down: usize) -> usize {
    let mut p = Point(0, 0);
    let mut counter = 0;
    while p.1 < grid.len() - 1 {
        p.0 += right;
        if p.0 > grid[0].len() - 1 {
            p.0 %= grid[0].len();
        }
        p.1 += down;

        if grid[p.1][p.0] {
            counter += 1;
        }
    }
    counter
}

#[aoc(day3, part2)]
pub fn part2(grid: &[Vec<bool>]) -> usize {
    let mut res = 1;

    for i in &[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)] {
        res *= count_trees(grid, i.0, i.1);
    }

    res // 2122848000
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_examples() {
        let grid = input_generator("..##.......\n#...#...#..\n.#....#..#.\n..#.#...#.#\n.#...##..#.\n..#.##.....\n.#.#.#....#\n.#........#\n#.##...#...\n#...##....#\n.#..#...#.#\n");
        assert_eq!(count_trees(&grid, 3, 1), 7);
    }

    #[test]
    fn part2_examples() {
        let grid = input_generator("..##.......\n#...#...#..\n.#....#..#.\n..#.#...#.#\n.#...##..#.\n..#.##.....\n.#.#.#....#\n.#........#\n#.##...#...\n#...##....#\n.#..#...#.#\n");
        assert_eq!(count_trees(&grid, 1, 1), 2);
        assert_eq!(count_trees(&grid, 5, 1), 3);
        assert_eq!(count_trees(&grid, 7, 1), 4);
        assert_eq!(count_trees(&grid, 1, 2), 2);
    }
}
