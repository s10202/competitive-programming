package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

/**
 * Day20Test
 */
public class Day20Test {    
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(20);

    Day20 day;

    @Before
    public void setup() {
        day = new Day20();
    }

    @Test
    public void sampleOne() {
        List<String> in = new ArrayList<>() {{
                add("p=< 3,0,0>, v=< 2,0,0>, a=<-1,0,0>");
                add("p=< 4,0,0>, v=< 0,0,0>, a=<-2,0,0>");
            }
        };

        assertEquals(0, (int)day.part1(in));
    }

    @Test
    public void sampleTwo() {
        List<String> in = new ArrayList<>() {{
                add("p=<-6,0,0>, v=< 3,0,0>, a=< 0,0,0>");
                add("p=<-4,0,0>, v=< 2,0,0>, a=< 0,0,0>");
                add("p=<-2,0,0>, v=< 1,0,0>, a=< 0,0,0>");
                add("p=< 3,0,0>, v=<-1,0,0>, a=< 0,0,0>");
            }
        };

        assertEquals(1, (int)day.part2(in));
    }
    
    @Test
    public void partOne() {
        assertEquals(300, (int) day.part1(input.getLines()));
    }

    @Test
    public void partTwo() { 
        assertEquals(502, (int)day.part2(input.getLines()));       
    }
}
