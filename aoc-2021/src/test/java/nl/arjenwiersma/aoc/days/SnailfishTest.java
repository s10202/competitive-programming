package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class SnailfishTest {

    @Test
    public void iCanParse() {

        Snailfish sf = Snailfish.parse("[1,2]");
        assertEquals(1L, (long) sf.left.value);
        assertEquals(2L, (long) sf.right.value);

        sf = Snailfish.parse("[[1,9],[8,5]]");

        assertEquals(1L, (long) sf.left.left.value);
        assertEquals(9L, (long) sf.left.right.value);

        assertEquals(sf.hashCode(), sf.left.root.hashCode());

        assertEquals(8L, (long) sf.right.left.value);
        assertEquals(5L, (long) sf.right.right.value);
        sf = Snailfish.parse("[9,[8,7]]");

        assertEquals(9L, (long) sf.left.value);
        assertEquals(8L, (long) sf.right.left.value);
        assertEquals(7L, (long) sf.right.right.value);        
    }

    @Test
    public void iCanAdd() {
        // [1,2] + [[3,4],5]
        Snailfish sfl = Snailfish.parse("[1,2]");
        Snailfish sfr = Snailfish.parse("[[3,4],5]");
        // [[1,2],[[3,4],5]]
        assertEquals("[[1,2],[[3,4],5]]", sfl.add(sfr).toString());
    }

    @Test
    public void iCanReduce() {
        Map<String, String> tests = new HashMap<>() {{
                put("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]");
                put("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]");
                put("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]");
                put("[[[[4,3],4],4],[7,[[8,4],9]]]", "[[[[4,3],4],4],[7,[[8,4],9]]]");
                put("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]");
            }
        };

        Snailfish sf = Snailfish.parse("[[[[4,3],4],4],[7,[[8,4],9]]]");
        tests.put(sf.add(Snailfish.parse("[1,1]")).toString(), "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");

        for (String k : tests.keySet()) {
            sf = Snailfish.parse(k);
            sf.reduce();
            assertEquals(tests.get(k), sf.toString());
        }
        
    }
}
