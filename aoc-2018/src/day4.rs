use lazy_static::lazy_static;
use regex::Regex;
use std::{collections::HashMap, str::FromStr};

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
enum State {
    Begin,
    Sleep,
    Wakeup,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Entry {
    year: u32,
    month: u32,
    day: u32,
    hour: u32,
    minute: u32,
    state: State,
    id: u32,
}

impl FromStr for Entry {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex =
                Regex::new(r"\[(\d+)-(\d+)-(\d+) (\d+):(\d+)\] (:?Guard #(\d+)|(falls)|(wakes))")
                    .unwrap();
        }

        let cap = RE.captures(s).unwrap();

        if cap.len() != 10 {
            return Err(anyhow::format_err!("Wrong format!"));
        }

        let year: u32 = cap[1].parse().unwrap();
        let month: u32 = cap[2].parse().unwrap();
        let day: u32 = cap[3].parse().unwrap();
        let hour: u32 = cap[4].parse().unwrap();
        let minute: u32 = cap[5].parse().unwrap();

        let state = match &cap[6] {
            "falls" => State::Sleep,
            "wakes" => State::Wakeup,
            _ => State::Begin,
        };

        let id = match state {
            State::Begin => cap[7].parse().unwrap(),
            State::Sleep => 0,
            State::Wakeup => 0,
        };

        Ok(Entry {
            year,
            month,
            day,
            hour,
            minute,
            state,
            id,
        })
    }
}

#[aoc_generator(day4)]
fn input_generator(input: &str) -> Vec<Entry> {
    input.split('\n').filter_map(|l| l.parse().ok()).collect()
}

fn process_inputs(input: &[Entry]) -> HashMap<u32, HashMap<u32, u32>> {
    let mut data = input.to_vec();
    data.sort_unstable_by_key(|item| (item.year, item.month, item.day, item.hour, item.minute));

    // Bepaal de frequency per minuut voor elke guard
    //
    // Meeste minuten
    // Welke minuut het meeste
    let mut guards: HashMap<u32, HashMap<u32, u32>> = HashMap::new();

    let mut first_minute = 0;
    let mut current_guard = 0;
    for entry in data {
        match entry.state {
            State::Begin => current_guard = entry.id,
            State::Sleep => first_minute = entry.minute,
            State::Wakeup => {
                guards.entry(current_guard).or_insert_with(HashMap::new);

                let mins = guards.get_mut(&current_guard).unwrap();
                for m in first_minute..entry.minute {
                    *mins.entry(m).or_insert(0) += 1;
                }
            }
        }
    }

    guards
}

fn get_max_by_value(input: HashMap<u32, u32>) -> u32 {
    input
        .iter()
        .max_by(|a, b| a.1.cmp(b.1))
        .map(|(k, _v)| k)
        .unwrap()
        .to_owned()
}

#[aoc(day4, part1)]
fn part1(input: &[Entry]) -> u32 {
    let guards = process_inputs(input);

    let mut c: HashMap<u32, u32> = HashMap::new();

    for guard in guards.clone() {
        let mut count = 0;
        for min in guard.1 {
            count += min.1;
        }
        c.entry(guard.0).or_insert(count);
    }

    let g = get_max_by_value(c);
    let v = guards.get(&g).unwrap().to_owned();
    let m = get_max_by_value(v);

    m * g // 146622
}

#[aoc(day4, part2)]
fn part2(input: &[Entry]) -> u32 {
    let guards = process_inputs(input);

    // voor alle guards, wat is het hoogste aantal keren slapen per minuut?
    // (guardid, max keren van minuten, minuut met hoogste count)
    let mut maxes: Vec<(u32, u32, u32)> = Vec::new();
    for guard in guards {
        let (min, times) = guard
            .1
            .iter()
            .max_by(|a, b| a.1.cmp(b.1))
            .map(|(k, v)| (k, v))
            .unwrap();
        maxes.push((guard.0, *min, *times));
    }

    maxes.sort_unstable_by_key(|item| item.2);
    maxes.reverse();
    let res = maxes.first().unwrap();
    res.0 * res.1 // 31848
}

#[cfg(test)]
mod tests {
    use crate::day4::*;

    #[test]
    fn examples_part1() {
        let input = input_generator(
            "[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up",
        );

        assert_eq!(part1(&input), 240);
    }

    #[test]
    fn examples_part2() {
        let input = input_generator(
            "[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up",
        );

        assert_eq!(part2(&input), 4455);
    }
}
