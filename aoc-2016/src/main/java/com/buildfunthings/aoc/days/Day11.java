package com.buildfunthings.aoc.days;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.buildfunthings.aoc.common.Day;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import lombok.ToString;

public class Day11 implements Day<Integer> {
    private enum TYPE { generator, microchip }

    @ToString
    private class Item {
        String name;
        TYPE type;

        public Item(String group, TYPE type) {
            this.name = group;
            this.type = type;
        }
    }

    // any -> stream anyMatch
    // all -> stream allMatch

    List<Set<Item>> parseFloors(List<String> floors) {
        Pattern p = Pattern.compile("(\\w+)(?:-compatible)? (microchip|generator)");

        List<Set<Item>> fl = new ArrayList<>();       
        
        for (String f : floors) {
            Set<Item> items = new HashSet<Item>();
            Matcher m = p.matcher(f);
            while (m.find()) {
                Item i = new Item(m.group(1), TYPE.valueOf(m.group(2)));
                items.add(i);
            }
            fl.add(items);
        }
        return fl;
    }

    public static<T> Set<T> combine(Set<T>... sets) {
        return Sets.newHashSet(Iterables.concat(sets));
    }
    
    @Override
    public Integer part1(List<String> input) {
        var floors = parseFloors(input);
        Set<Set<Item>> r = Sets.combinations(floors.get(0), 2);
        Set<Set<Item>> r2 = Sets.combinations(floors.get(0), 1);
        Set<Set<Item>> y = combine(r, r2);
        System.out.println(y);
        return 0;
    }

    @Override
    public Integer part2(List<String> input) {
        return 0;
    }
}
