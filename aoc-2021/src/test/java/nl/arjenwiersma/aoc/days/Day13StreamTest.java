package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day13StreamTest {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(13);

    Day13Stream day;

    @Before
    public void before() {
        day = new Day13Stream();
    }

    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {
            {
                add("6,10");
                add("0,14");
                add("9,10");
                add("0,3");
                add("10,4");
                add("4,11");
                add("6,0");
                add("6,12");
                add("4,1");
                add("0,13");
                add("10,12");
                add("3,4");
                add("3,0");
                add("8,4");
                add("1,10");
                add("2,14");
                add("8,10");
                add("9,0");
                add("");
                add("fold along y=7");
                add("fold along x=5");
            }
        };
        assertEquals(17, (int) day.part1(input));
    }

    @Test
    public void part1() {
        assertEquals(785, (int) day.part1(input.getLines()));
    }

    @Test
    public void part2() {
        assertEquals(0, (int) day.part2(input.getLines()));
    }
}
