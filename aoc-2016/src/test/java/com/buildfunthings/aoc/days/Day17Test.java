package com.buildfunthings.aoc.days;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import com.buildfunthings.aoc.common.Day;
import com.buildfunthings.aoc.common.DayInputExternalResource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class Day17Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(17);

    Day<String> day;
    
    @Before
    public void before() {
        day = new Day17();
    }

    @Test
    public void testMd5() {
        Day17 d = (Day17) day;

        String md5 = d.produceMD5("hijkl", "");

        assertTrue(md5.startsWith("ced9"));

        md5 = d.produceMD5("hijkl", "D");
        assertTrue(md5.startsWith("f2bc"));
    }

    @Test
    public void testStates() {
        Day17 d = (Day17) day;
        
        String md5 = d.produceMD5("hijkl", "");
        boolean[] doors = d.doorStates(md5);
        
        assertArrayEquals(new boolean[] { true, true, true, false }, doors);

        md5 = d.produceMD5("hijkl", "D");
        doors = d.doorStates(md5);

        assertArrayEquals(new boolean[] { true, false, true, true }, doors);
        
    }
    
    @Test
    public void testSample() {
        assertEquals("DDRRRD", day.part1(new ArrayList<>() {{ add("ihgpwlah"); }}));
        assertEquals("DDUDRLRRUDRD", day.part1(new ArrayList<>() {{ add("kglvqrro"); }}));
        assertEquals("DRURDRUDDLLDLUURRDULRLDUUDDDRR", day.part1(new ArrayList<>() {{ add("ulqzkmiv"); }}));

        assertEquals("370", day.part2(new ArrayList<>() {{ add("ihgpwlah"); }}));
        assertEquals("492", day.part2(new ArrayList<>() {{ add("kglvqrro"); }}));
        assertEquals("830", day.part2(new ArrayList<>() {{ add("ulqzkmiv"); }}));
    
    }

    @Test
    public void part1() {
        assertEquals("RDULRDDRRD", day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals("752", day.part2(input.getLines()));
    }
}
