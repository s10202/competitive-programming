package nl.arjenwiersma.aoc.utils;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;


/**
 * ArrayUtilsTest
 */
public class ArrayUtilsTest {
    
    @Test
    public void testInterleave() {
        assertEquals(List.of(List.of(1,2), List.of(2,3)), ArrayUtils.interleave(List.of(1,2,3),1,false));
        assertEquals(List.of(List.of(1, 2), List.of(2, 3), List.of(3, 1)),
                     ArrayUtils.interleave(List.of(1, 2, 3), 1, true));

        assertEquals(List.of(List.of(1,1), List.of(2,2), List.of(1,1), List.of(2,2)),
                     ArrayUtils.interleave(List.of(1,2,1,2),2,true));
    }

    @Test
    public void testRotateClockWise() {
        char[][] grid = new char[][] {
            {'.', '.', '.'},
            {'#', '#', '#'},
            {'*', '*', '*'},
        };

        assertEquals(new char[][]{
            {'*', '#', '.'},
            {'*', '#', '.'},
            {'*', '#', '.'},
            }, ArrayUtils.rotateClockWise(grid));
        assertEquals(new char[][]{
            {'*', '*', '*'},
            {'#', '#', '#'},
            {'.', '.', '.'},
            }, ArrayUtils.rotateClockWise(ArrayUtils.rotateClockWise(grid)));
    }

    @Test
    public void testFlip() {
        char[][] grid = new char[][] {
            {'*', '.', '.'},
            {'#', '#', '#'},
            {'&', '*', '*'},
        };

        ArrayUtils.flip(grid);
        assertEquals(new char[][]{
            {'.', '.', '*'},
            {'#', '#', '#'},
            {'*', '*', '&'},
            }, grid);
    }
    
}
