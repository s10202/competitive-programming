package nl.arjenwiersma.aoc.days;

import java.util.List;

import nl.arjenwiersma.aoc.common.NewDay;

record Pair(int l, int r) {
};

public class Day02 extends NewDay<List<Pair>, Integer> {

    @Override
    public List<Pair> parseInput(List<String> input) {
        return input.stream().map(l -> {
            int me = switch (l.charAt(0)) {
            case 'A' -> 1;
            case 'B' -> 2;
            case 'C' -> 3;
            default -> 0;
            };
            int them = switch (l.charAt(2)) {
            case 'X' -> 1;
            case 'Y' -> 2;
            case 'Z' -> 3;
            default -> 0;
            };
            return new Pair(me, them);
        }).toList();
    }

    private int play(int me, int them) {
        if (me == 1 && them == 2) {
            return me; // rock vs paper
        } else if (me == 2 && them == 1) {
            return me + 6; // paper vs rock
        } else if (me == 1 && them == 3) {
            return me + 6; // rock vs scissors
        } else if (me == 3 && them == 1) {
            return me; // scissors vs rock
        } else if (me == 2 && them == 3) {
            return me; // paper vs scissors
        } else if (me == 3 && them == 2) {
            return me + 6; // scissors vs paper
        } else if (me == them) {
            return me + 3;
        }
        return 0;
    }

    private int play2(int outcome, int them) {
        if (outcome == 1) { // lose
            return switch (them) {
            case 1 -> 3;
            case 2 -> 1;
            case 3 -> 2;
            default -> 0;
            };
        } else if (outcome == 2) { // draw
            return switch (them) {
            case 1 -> 4;
            case 2 -> 5;
            case 3 -> 6;
            default -> 0;
            };
        } else if (outcome == 3) { // win
            return switch (them) {
            case 1 -> 8;
            case 2 -> 9;
            case 3 -> 7;
            default -> 0;
            };
        }
        return 0;
    }

    @Override
    public Integer solver1(List<Pair> input) {
        Integer score = 0;

        for (Pair play : input) {
            score += play(play.r(), play.l());
        }
        return score;
    }

    @Override
    public Integer solver2(List<Pair> input) {
        Integer score = 0;

        for (Pair play : input) {
            score += play2(play.r(), play.l());
        }
        return score;
    }

}
