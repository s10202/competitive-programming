use std::collections::HashMap;

#[aoc_generator(day6)]
fn input_generator(input: &str) -> Vec<Vec<String>> {
    input
        .split("\n\n")
        .map(|l| l.split('\n').map(|s| s.to_string()).collect())
        .collect()
}

#[aoc(day6, part1)]
fn part1(input: &[Vec<String>]) -> usize {
    let mut counter = 0;
    for group in input {
        let mut seen = HashMap::new();
        for line in group {
            for c in line.chars() {
                *seen.entry(c).or_insert(0) += 1;
            }
        }
        counter += seen.len();
    }
    counter // 6565
}

#[aoc(day6, part2)]
fn part2(input: &[Vec<String>]) -> usize {
    let mut counter = 0;
    for group in input {
        let mut seen = HashMap::new();
        for line in group {
            for c in line.chars() {
                *seen.entry(c).or_insert(0) += 1;
            }
        }

        let s = group.len();
        for k in seen.keys() {
            if *seen.get(k).unwrap() == s {
                counter += 1;
            }
        }
    }
    counter // 3137
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let i = input_generator(
            "abc

a
b
c

ab
ac

a
a
a
a

b",
        );

        assert_eq!(part1(&i), 11);
    }

    #[test]
    fn test_part2() {
        let i = input_generator(
            "abc

a
b
c

ab
ac

a
a
a
a

b",
        );

        assert_eq!(part2(&i), 6);
    }
}
