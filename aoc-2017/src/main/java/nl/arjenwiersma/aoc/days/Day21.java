package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.Collections2;

import nl.arjenwiersma.aoc.common.Day;

/**
 * Day21
 */
public class Day21 extends Day<Map<String, String>, Long> {

    @Override
    public Map<String, String> parseInput(List<String> input) {
        Map<String, String> rules = new HashMap<>();
        input.stream().forEach(s -> {
                String[] p = s.split(" => ");
                String[] parts = p[0].split("/");
                Collection<List<String>> perms = Collections2.permutations(Arrays.stream(parts).toList());
                System.out.println("Permutations");
                for (List<String> ps : perms) {
                    String result = ps.stream().collect(Collectors.joining("/"));
                    rules.put(result, p[1]);
                }
                //rules.put(p[0], p[1]);
            });
        return rules;
    }

    
    @Override
    public Long solver1(Map<String, String> rules) {
        // char[][] def = new char[][] {
        //     {'.','#','.'},
        //     {'.','.','#'},
        //     {'#','#','#'},
        // };
        
        // System.out.println(enhance(def));
        String def = ".#./..#/###";

        List<String> grid = new ArrayList<>();
        grid.add(def);

        for (int i = 0; i < 2; i++) {
            grid = expand(rules, grid);
            printGrid(grid);
        }

        System.out.println(grid);
        
        return null;
    }

    private void printGrid(List<String> grid) {
        for (String s : grid) {
            System.out.println(s.replaceAll("/", "\n"));
        }
    }


    private List<String> expand(Map<String, String> rules, List<String> grid) {
        List<String> newGrid = new ArrayList<>();
        for (String s : grid) {
            if (s.length() == 20) {
                String[] parts = new String[] {
                    ""+s.charAt(0)+s.charAt(1)+"/"+s.charAt(5)+s.charAt(6),
                    ""+s.charAt(2)+s.charAt(3)+"/"+s.charAt(7)+s.charAt(8),
                    ""+s.charAt(10)+s.charAt(11)+"/"+s.charAt(15)+s.charAt(16),
                    ""+s.charAt(12)+s.charAt(13)+"/"+s.charAt(17)+s.charAt(18),
                };
                for (String p : parts) {
                    newGrid.add(rules.get(p));
                }
            } else {
                newGrid.add(rules.get(s));
            }
        }
        System.out.println(newGrid);
        return newGrid;
    }


    @Override
    public Long solver2(Map<String, String> input) {
        return null;
    }
    
 
}
