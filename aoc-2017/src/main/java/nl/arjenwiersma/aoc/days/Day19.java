package nl.arjenwiersma.aoc.days;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import nl.arjenwiersma.aoc.common.Day;

/**
 * Day19
 */
public class Day19 extends Day<char[][], String> {

    @Override
    public char[][] parseInput(List<String> input) {
        char[][] grid = new char[input.size()][input.get(0).length()];

        for (int index = 0; index < input.size(); index++) {
            grid[index] = input.get(index).toCharArray();
        }
        
        return grid;
    }

    private String solve(char[][] input, boolean part2) {
        int y = 0;
        int x = IntStream.range(0, input[0].length)
            .filter(z -> input[0][z] == '|')
            .findFirst()
            .orElse(-1);

        int h = input.length;
        int w = input[0].length;

        /**
         * Directions are y/x coords: down, left, up, right
         */
        int[][] directions = new int[][] { { 1, 0 }, { 0, -1 }, { -1, 0 }, { 0, 1 } };
        int direction = 0; // start down
        
        StringBuilder chars = new StringBuilder();
        int steps = 0;
        while (x >= 0 && x < w && y >= 0 && y < h && input[y][x] != ' ') {
            switch (input[y][x]) {
            case '|', '-' -> {
                // no change in direction
            }
            case '+' -> {
                // A direction change is necessary
                // if up/down it will be left/right
                // if right/left it will be up/down

                if (direction == 0 || direction == 2) {
                    if (x > 0 && input[y][x-1] != ' ')
                        direction = 1; // left
                    else if (x < w-1 && input[y][x+1] != ' ')
                        direction = 3; // right
                    else {
                        System.out.println("Should change, but no idea where to");
                    }
                } else {
                    if (y < h-1 && input[y + 1][x] != ' ')
                        direction = 0; // move down
                    else if (y > 0 && input[y - 1][x] != ' ')
                        direction = 2; // move down
                    else {
                        System.out.println("Should change, but no idea where to");
                    }
                }
            }
            default -> {
                chars.append(input[y][x]);
            }
            }
            y += directions[direction][0];
            x += directions[direction][1];

            // precent endless loops
            steps++;
            if (steps > 100000) {
                System.out.println("Your code is no good yet :D");
                break;
            }
                
        }

        return part2 ? String.valueOf(steps) : chars.toString();
    }
    
    @Override
    public String solver1(char[][] input) {
        return solve(input, false);
    }

    @Override
    public String solver2(char[][] input) {
        return solve(input, true);
    }
    
    
}
