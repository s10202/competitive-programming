#[aoc_generator(day5)]
fn input_generator(input: &str) -> Vec<i32> {
    input.chars().map(|c| c as i32).collect()
}

// fn solve(left: &[char], acc: &[char]) -> u32 {
//     // if left.len() == 0 {
//     //     return acc;
//     // }
//     0
// }

fn solve(input: &[i32]) -> String {
    let mut answer = Vec::new();
    let mut drop_next = 0;
    for i in 0..input.len() {
        if i == input.len() - 1 {
            answer.push(input[i]);
            continue;
        }

        if drop_next > 0 {
            drop_next -= 1;
            continue;
        }

        // the last and current first collide
        if !answer.is_empty() && (answer.last().unwrap() - input[i]).abs() == 32 {
            let _res = answer.pop();
            continue;
        }

        if (input[i] - input[i + 1]).abs() != 32 {
            answer.push(input[i]);
        } else {
            drop_next = 1; // drop this char, and the next
        }
    }

    answer
        .iter()
        .map(|x| char::from_u32((*x).try_into().unwrap()).unwrap())
        .collect::<String>()
}

#[aoc(day5, part1)]
fn part1(input: &[i32]) -> usize {
    solve(input).len() // 9078
}

#[aoc(day5, part2)]
fn part2(input: &[i32]) -> usize {
    let mut min = usize::MAX;
    for b in 65..91 {
        let filtered: Vec<i32> = input
            .iter()
            .filter(|x| !(**x == b || **x == b + 32))
            .map(|x| x.to_owned())
            .collect();

        let res = solve(filtered.as_slice());
        if res.len() < min {
            min = res.len();
        }
    }

    min // 5698
}

#[cfg(test)]
mod tests {
    use crate::day5::*;

    #[test]
    fn examples_part1() {
        let input = input_generator("dabAcCaCBAcCcaDA");
        assert_eq!(part1(&input), 10);
    }

    #[test]
    fn examples_part2() {
        let input = input_generator("dabAcCaCBAcCcaDA");
        assert_eq!(part2(&input), 4);
    }
}
