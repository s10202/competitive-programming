use std::collections::HashMap;

#[aoc_generator(day2)]
pub fn input_generator(input: &str) -> Vec<String> {
    let things: Vec<String> = input.lines().map(|s| s.to_string()).collect();
    things
}

fn count_letters(word: &str) -> (usize, usize) {
    let mut letters: HashMap<char, i32> = HashMap::new();

    let ch: Vec<char> = word.to_string().chars().collect();
    for c in ch {
        *letters.entry(c).or_insert(0) += 1
    }
    let d = letters.iter().filter(|(_k, v)| **v == 2).count();
    let t = letters.iter().filter(|(_k, v)| **v == 3).count();
    (d, t)
}

#[aoc(day2, part1)]
pub fn part1(input: &[String]) -> u32 {
    let mut double = 0;
    let mut triple = 0;

    for s in input {
        let counts = count_letters(s);
        //println!("{} {:?}", s, counts);
        if counts.0 > 0 {
            double += 1;
        }
        if counts.1 > 0 {
            triple += 1;
        }
    }

    double * triple // 5681
}

fn hamming_distance(s1: &str, s2: &str) -> usize {
    let mut distance: usize = 0;

    let mut chars1 = s1.chars();
    let mut chars2 = s2.chars();

    loop {
        match (chars1.next(), chars2.next()) {
            (None, None) => break,
            (None, Some(_)) => panic!("length of strings is not equal!"),
            (Some(_), None) => panic!("length of strings is not equal!"),
            (Some(x), Some(y)) if x == y => continue,
            (Some(x), Some(y)) if x != y => distance += 1,
            _ => unreachable!(),
        }
    }

    distance
}

fn common_chars(s1: &str, s2: &str) -> String {
    let mut common: Vec<char> = Vec::new();

    let mut chars1 = s1.chars();
    let mut chars2 = s2.chars();

    loop {
        match (chars1.next(), chars2.next()) {
            (None, None) => break,
            (None, Some(_)) => panic!("length of strings is not equal!"),
            (Some(_), None) => panic!("length of strings is not equal!"),
            (Some(x), Some(y)) if x == y => common.push(x),
            (Some(x), Some(y)) if x != y => continue,
            _ => unreachable!(),
        }
    }

    common.iter().collect()
}

#[aoc(day2, part2)]
pub fn part2(input: &[String]) -> String {
    let n = input.len();
    for i in 0..n {
        for x in i..n {
            if hamming_distance(&input[i], &input[x]) == 1 {
                let common = common_chars(&input[i], &input[x]);

                return common; // uqyoeizfvmbistpkgnocjtwld
            }
        }
    }

    String::from("nope")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_example() {
        let i = input_generator(
            "abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab",
        );
        assert_eq!(part1(&i), 12);
    }

    #[test]
    fn part2_example() {
        let i = input_generator(
            "abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz",
        );
        assert_eq!(part2(&i), "fgij")
    }
}
