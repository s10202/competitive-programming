package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day05Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(05);

    Day05 day;

    @Before
    public void before() {
        day = new Day05();
    }

    @Test
    public void testSample() {
        String start = """
                    [D]
                [N] [C]
                [Z] [M] [P]
                 1   2   3

                move 1 from 2 to 1
                move 3 from 1 to 3
                move 2 from 2 to 1
                move 1 from 1 to 2
                """;

        List<String> input = Arrays.stream(start.split("\n")).toList();
        assertEquals("CMZ", day.part1(input));
    }

    @Test
    public void part1() {
        assertEquals("RTGWZTHLD", day.part1(input.getLines()));
    }

    @Test
    public void part2() {
        assertEquals("STHGRZZFR", day.part2(input.getLines()));
    }
}
