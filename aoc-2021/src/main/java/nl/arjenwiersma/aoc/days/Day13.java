package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nl.arjenwiersma.aoc.common.Day;
import nl.arjenwiersma.aoc.utils.Coord;

public class Day13 implements Day<Integer> {

    @Override
    public Integer part1(List<String> input) {
        int[][] points = new int[input.size()][2];

        List<Coord> board = new ArrayList<>();
        for (String in : input) {
            if (in.isEmpty())
                continue;
            if (!in.startsWith("fold")) {
                int[] c = Arrays.stream(in.trim().split(",")).mapToInt(Integer::parseInt).toArray();
                
                board.add(new Coord(c[0], c[1]));
            } else {
                String[] p = in.substring(11).trim().split("=");
                int offset = Integer.parseInt(p[1]);

                List<Coord> prune = new ArrayList<>();
                List<Coord> toAdd = new ArrayList<>();
                for (Coord c : board) {
                    switch (p[0]) {
                    case "y" -> {
                        if (c.y() > offset) {
                            prune.add(c);
                            Coord n = new Coord(c.x(), (offset -  (c.y() - offset)));
                            if (!board.contains(n)) {
                                toAdd.add(n);
                            }
                        }
                    }
                    case "x" -> {
                        if (c.x() > offset) {
                            prune.add(c);
                            Coord n = new Coord((offset -  (c.x() - offset)), c.y());
                            if (!board.contains(n)) {
                                toAdd.add(n);
                            }
                        }
                    }
                    }
                }
                board.removeAll(prune);
                board.addAll(toAdd);

                break;
            }

        }


        return board.size();
    }

    @Override
    public Integer part2(List<String> input) {
        int[][] points = new int[input.size()][2];

        List<Coord> board = new ArrayList<>();
        for (String in : input) {
            if (in.isEmpty())
                continue;
            if (!in.startsWith("fold")) {
                int[] c = Arrays.stream(in.trim().split(",")).mapToInt(Integer::parseInt).toArray();
                
                board.add(new Coord(c[0], c[1]));
            } else {
                String[] p = in.substring(11).trim().split("=");
                int offset = Integer.parseInt(p[1]);

                List<Coord> prune = new ArrayList<>();
                List<Coord> toAdd = new ArrayList<>();
                for (Coord c : board) {
                    switch (p[0]) {
                    case "y" -> {
                        if (c.y() > offset) {
                            prune.add(c);
                            Coord n = new Coord(c.x(), (offset -  (c.y() - offset)));
                            if (!board.contains(n)) {
                                toAdd.add(n);
                            }
                        }
                    }
                    case "x" -> {
                        if (c.x() > offset) {
                            prune.add(c);
                            Coord n = new Coord((offset -  (c.x() - offset)), c.y());
                            if (!board.contains(n)) {
                                toAdd.add(n);
                            }
                        }
                    }
                    }
                }
                board.removeAll(prune);
                board.addAll(toAdd);
                
            }

        }

        int maxX = board.stream().mapToInt(x -> x.x()).max().getAsInt();
        int maxY = board.stream().mapToInt(x -> x.y()).max().getAsInt();

        for (int y = 0; y <= maxY; y++) {
            for (int x = 0; x <= maxX; x++) {
                System.out.print(board.contains(new Coord(x, y)) ? "#" : ".");
            }
            System.out.println();
        }
        
        return 0;
    }
    
}
