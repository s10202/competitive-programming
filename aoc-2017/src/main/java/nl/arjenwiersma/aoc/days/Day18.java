package nl.arjenwiersma.aoc.days;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.arjenwiersma.aoc.common.Day;

/**
 * Day18
 */
public class Day18 extends Day<List<Day18.Instruction>, Long> {
    
    public enum Inst {
        SND, SET, ADD, MUL, MOD, RCV, JGZ
    }
    
    public class Instruction {
        Inst type;
        String[] params;
        
        public Instruction(Inst type, String[] params) {
            this.type = type;
            this.params = params;
        }

        @Override
        public String toString() {
            return "Instruction [params=" + Arrays.toString(params) + ", type=" + type + "]";
        }
    }

    public Long getValue(Map<String, Long> register, String registerOrValue) {
        if (register.containsKey(registerOrValue)) {
            return register.get(registerOrValue);
        }

        try {
            return Long.parseLong(registerOrValue);            
        } catch (NumberFormatException ex) {
            return 0L;           // default value
        }
    }
    
    @Override
    public List<Instruction> parseInput(List<String> input) {
        List<Instruction> instructions = new ArrayList<>();

        for (String in : input) {
            String[] parts = in.split(" ");
            instructions.add(switch (parts[0]) {
                case "snd" -> new Instruction(Inst.SND, Arrays.copyOfRange(parts, 1, 2));
                case "set" -> new Instruction(Inst.SET, Arrays.copyOfRange(parts, 1, 3));
                case "add" -> new Instruction(Inst.ADD, Arrays.copyOfRange(parts, 1, 3));
                case "mul" -> new Instruction(Inst.MUL, Arrays.copyOfRange(parts, 1, 3));
                case "mod" -> new Instruction(Inst.MOD, Arrays.copyOfRange(parts, 1, 3));
                case "rcv" -> new Instruction(Inst.RCV, Arrays.copyOfRange(parts, 1, 2));
                case "jgz" -> new Instruction(Inst.JGZ, Arrays.copyOfRange(parts, 1, 3));
                default -> null;
                }
                );
        }
        return instructions;
    }

    @Override
    public Long solver1(List<Instruction> input) {
        Map<String, Long> register = new HashMap<>();
        Long lastSound = 0L;
        for (int ip = 0; ip < input.size();) {
            Instruction i = input.get(ip);
            ip++; // setup for the next instruction

            switch (i.type) {
            case ADD -> {
                Long c = getValue(register, i.params[0]);
                Long v = getValue(register, i.params[1]);
                
                register.put(i.params[0], c+v);
            }
            case JGZ -> {
                Long c = getValue(register, i.params[0]);
                Long v = getValue(register, i.params[1]);

                if (c > 0) {
                    ip += v - 1;
                }
            }
            case MOD -> {
                Long c = getValue(register, i.params[0]);
                Long v = getValue(register, i.params[1]);
                
                register.put(i.params[0], c%v);
            }
            case MUL -> {
                Long c = getValue(register, i.params[0]);
                Long v = getValue(register, i.params[1]);
                
                register.put(i.params[0], c*v);
            }
            case RCV -> {
                Long v = getValue(register, i.params[0]);
                if (v != 0) {
                    return lastSound;
                }
            }
            case SET -> {
                Long v = getValue(register, i.params[1]);
                register.put(i.params[0], v);
            }
            case SND -> {
                Long v = getValue(register, i.params[0]);
                lastSound = v;
            }
            default -> throw new IllegalArgumentException("Unexpected value: " + i.type);
            }
        }
        return 0L;
    }

    private boolean isDeadlocked(int[] state) {
        return Arrays.stream(state).allMatch(x -> x == 1);
    }

    /**
     * Progress a program until it hits a receive (and no entries are in the queue)
     */
    private boolean progress(int index, List<Instruction> inst, int[] ip,
                          List<Map<String, Long>> registers, List<Deque<Long>> queue,
                          List<Long> sends, int[] state) {
        //System.out.println("Progressing " + index + " ip: " + Arrays.toString(ip) + " sends: " + sends);
        Map<String, Long> register = registers.get(index);
        Deque<Long> q = queue.get(index);
        Deque<Long> otherq = queue.get(index == 0 ? 1 : 0);
        
        while (ip[index] < inst.size()) {
            Instruction in = inst.get(ip[index]);
            ip[index]++;
            
            switch (in.type) {
            case ADD -> {
                Long c = getValue(register, in.params[0]);
                Long v = getValue(register, in.params[1]);
                
                register.put(in.params[0], c+v);
            }
            case JGZ -> {
                Long c = getValue(register, in.params[0]);
                Long v = getValue(register, in.params[1]);

                if (c > 0) {
                    ip[index] += v - 1;
                }
            }
            case MOD -> {
                Long c = getValue(register, in.params[0]);
                Long v = getValue(register, in.params[1]);
                
                register.put(in.params[0], c%v);
            }
            case MUL -> {
                Long c = getValue(register, in.params[0]);
                Long v = getValue(register, in.params[1]);
                
                register.put(in.params[0], c*v);
            }
            case RCV -> {
                //System.out.println("Receiving in program : " + index + " sends: " + sends);
                state[index] = 0;
                if (q.isEmpty()) {
                    //System.out.println("Queue is empty... deadlock: " + q);
                    state[index] = 1;
                    ip[index]--;
                    return true;
                } else {
                    Long v = q.removeLast();
                    //System.out.println("Queue is not empty... value: " + v);
                    register.put(in.params[0], v);
                }
            }
            case SET -> {
                Long v = getValue(register, in.params[1]);
                register.put(in.params[0], v);
            }
            case SND -> {
                Long v = getValue(register, in.params[0]);
                //                System.out.println("Sending from program: " + index + " value " + v);
                sends.set(index, sends.get(index) + 1);
                otherq.addFirst(v);
                //System.out.println("Queues: This " + q + " Other " + otherq);
            }
            default -> throw new IllegalArgumentException("Unexpected value: " + in.type);
            }
        }
        state[index] = 1;
        return false;
    }
    
    @Override
    public Long solver2(List<Instruction> input) {
        int[] ip = new int[] { 0, 0 };
        List<Map<String, Long>> registers = new ArrayList<>() {{
                add(0, new HashMap<>() {{
                        put("p", 0L);
                }});
                add(1, new HashMap<>()  {{
                        put("p", 1L);
                }});
            }
        };
        List<Deque<Long>> queue = new ArrayList<>() {{
                add(new ArrayDeque<>(0));
                add(new ArrayDeque<>(0));
            }
        };
        List<Long> sends = new ArrayList<>() {{
                add(0L);
                add(0L);
            }
        };
        
        // 1 is waiting for rcv, 0 ready to go, when both are 1 a deadlock occurs
        int[] state = new int[] { 0, 0 };

        while (!isDeadlocked(state)) {
            for (int index = 0; index < 2; index++) {
                progress(index, input, ip, registers, queue, sends, state);
            }
            state[0] = queue.get(0).isEmpty() ? state[0] : 0;
            state[1] = queue.get(1).isEmpty() ? state[1] : 0;
        }

        //System.out.println(sends);
        return sends.get(1);
    }
   
}
