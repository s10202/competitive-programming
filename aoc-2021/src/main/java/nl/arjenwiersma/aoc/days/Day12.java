package nl.arjenwiersma.aoc.days;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.arjenwiersma.aoc.common.Day;

public class Day12 implements Day<Integer> {

    private List<List<String>> findPath(List<String> input, boolean smallCavesAllowed) {
        Map<String, List<String>> c = new HashMap<>();

        for (String in : input) {
            String[] parts = in.trim().split("-");

            List<String> current = c.getOrDefault(parts[0], new ArrayList<String>());
            current.add(parts[1]);
            c.put(parts[0], current);

            List<String> c2 = c.getOrDefault(parts[1], new ArrayList<String>());
            c2.add(parts[0]);
            c.put(parts[1], c2);

        }

        Deque<List<String>> q = new ArrayDeque<>();
        List<List<String>> paths = new ArrayList<>();

        List<String> start = new ArrayList<>() {
            {
                add("start");
            }
        };

        q.add(start);

        while (!q.isEmpty()) {
            List<String> path = q.pop();

            List<String> cc = c.get(path.get(path.size() - 1));

            if (cc == null) {
                if (Character.isUpperCase(path.get(path.size() - 2).charAt(0))) {
                    List<String> newPath = new ArrayList<>(path);
                    newPath.add(path.get(path.size() - 2));
                    q.add(newPath);
                }
            } else {
                ELEMS: for (String next : cc) {
                    if ("start".equals(next))
                        continue;

                    if (Character.isLowerCase(next.charAt(0))) {
                        if (path.contains(next)) {
                            if (!smallCavesAllowed) continue;
                            Map<String, Integer> freqs = new HashMap<>();
                            path.stream().forEach(x -> freqs.merge(x, 1, Integer::sum));
                            for (String k : freqs.keySet()) {
                                if (Character.isLowerCase(k.charAt(0)) && freqs.get(k) > 1) {
                                    continue ELEMS;
                                }
                            }
                        }
                    }                    

                    List<String> newPath = new ArrayList<>(path);
                    newPath.add(next);

                    if ("end".equals(next)) {
                        paths.add(newPath);
                    } else {
                        q.add(newPath);
                    }

                }
            }
        }
        return paths;
    }

    @Override
    public Integer part1(List<String> input) {
        List<List<String>> paths = findPath(input, false);

        return paths.size();
    }

    @Override
    public Integer part2(List<String> input) {
        List<List<String>> paths = findPath(input, true);

        return paths.size();
    }

}
