package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nl.arjenwiersma.aoc.common.NewDay;

public class Day13Stream extends NewDay<Day13Stream.Grid, Integer> {

    class Grid {
        Boolean[][] points;

        List<int[]> instructions = new ArrayList<>();
    }

    @Override
    public Grid parseInput(List<String> input) {
        Grid g = new Grid();
        boolean[][] temp = new boolean[2000][2000];

        int maxX = 0;
        int maxY = 0;
        for (String in : input) {
            if (in.startsWith("fold")) {
                String p[] = in.split(" ");
                String p2[] = p[2].split("=");

                g.instructions.add(new int[] { "x".equals(p2[0]) ? 0 : 1, Integer.parseInt(p2[1]) });
            } else {
                if (in.isBlank())
                    continue;

                int[] parts = Arrays.stream(in.split(",")).mapToInt(Integer::parseInt).toArray();
                maxX = Math.max(maxX, parts[0]);
                maxY = Math.max(maxY, parts[1]);

                temp[parts[1]][parts[0]] = true;
            }
        }

        g.points = new Boolean[maxY + 1][maxX + 1];
        for (int y = 0; y <= maxY; y++) {
            for (int x = 0; x <= maxX; x++) {
                g.points[y][x] = temp[y][x];
            }
        }

        return g;
    }

    private void printGrid(Grid input) {
        for (int y = 0; y < input.points.length; y++) {
            for (int x = 0; x < input.points[y].length; x++) {
                System.out.print(input.points[y][x] == true ? "#" : " ");
            }
            System.out.println();
        }
    }
    
    private long performInstructions(Grid input, boolean stopAtFirst) {
        for (int i = 0; i < input.instructions.size(); i++) {
            int[] inst = input.instructions.get(i);
            
            switch (inst[0]) {
            case 0 -> {
                int vouwlijn = inst[1];
                int newWidth = (input.points[0].length - (input.points[0].length % 2 == 0 ? 0 : 1)) - vouwlijn;
                Boolean[][] res = new Boolean[input.points.length][newWidth];

                for (int y = 0; y < input.points.length; y++) {
                    for (int x = 0; x < newWidth; x++) {
                        res[y][x] = input.points[y][x];
                    }
                }

                for (int y = 0; y < input.points.length; y++) {
                    for (int x = vouwlijn + 1; x < input.points[y].length; x++) {
                        int newX = vouwlijn - (x - vouwlijn);
                        if (input.points[y][x])
                            res[y][newX] = input.points[y][x];
                    }
                }
                input.points = res;
            }
            case 1 -> {
                int vouwlijn = inst[1];
                int newHeight = (input.points.length - (input.points.length % 2 == 0 ? 0 : 1)) - vouwlijn;
                Boolean[][] res = new Boolean[newHeight][input.points[0].length];

                for (int y = 0; y < newHeight; y++) {
                    for (int x = 0; x < input.points[y].length; x++) {
                        res[y][x] = input.points[y][x];
                    }
                }

                for (int y = vouwlijn + 1; y < input.points.length; y++) {
                    for (int x = 0; x < input.points[y].length; x++) {
                        int newY = vouwlijn - (y - vouwlijn);
                        if (input.points[y][x])
                            res[newY][x] = input.points[y][x];
                    }
                }
                input.points = res;
            }
            default -> System.out.println("GEEN IDEE WAT JE WIL");
            }

            if (stopAtFirst)
                return Arrays.stream(input.points).flatMap(xx -> Arrays.stream(xx)).filter(x -> x == true).count();

        }
        System.out.println("Resultaat:");
        printGrid(input);
        
        return Arrays.stream(input.points).flatMap(xx -> Arrays.stream(xx)).filter(x -> x == true).count();

    }
    
    @Override
    public Integer solver1(Grid input) {
        return (int) performInstructions(input, true);
    }

    @Override
    public Integer solver2(Grid input) {
        performInstructions(input, false);

        return 0;
    }



}
