package nl.arjenwiersma.aoc.days;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import nl.arjenwiersma.aoc.common.NewDay;

public class Day24 extends NewDay<Day24.Program, Long> {
    class Program {
        List<String> instructions;
    }

    class Machine {
        Map<String, Integer> registers = new HashMap<>();

        public boolean isValidSerial(String input, Program validator) {
            Deque<Character> q = new ArrayDeque<>();

            input.chars().mapToObj(x -> Character.valueOf((char) x)).forEach(x -> q.addLast(x)); // ugly

            for (String s : validator.instructions) {
                String[] tokens = s.split(" ");

                Integer a = registers.getOrDefault(tokens[1], 0);
                Integer b = tokens.length == 3 ? registerOrValue(tokens[2]) : 0;

                switch (tokens[0]) {
                case "inp" -> {
                    if (q.isEmpty()) {
                        System.out.println("Trying to pop a value that is not here");
                        return false;
                    }
                    registers.put(tokens[1], Integer.parseInt(""+q.pop()));
                }
                case "add" -> {
                    registers.put(tokens[1], a + b);                    
                }
                case "mul" -> {
                    registers.put(tokens[1], a * b);
                }
                case "div" -> {
                    if (b == 0) {
                      System.out.println("Division by 0!");
                      return false;
                    } 
                    registers.put(tokens[1], a / b);                    
                }
                case "mod" -> {
                    if (a < 0 || b <= 0) {
                      System.out.println("Invalid mod!: " + a + "  " + b);
                      return false;
                    } 
                    registers.put(tokens[1], a % b);                    
                }
                case "eql" -> {
                    registers.put(tokens[1], a == b ? 1 : 0);
                    
                }
                default -> {
                    System.out.println("Unkown instruction: " + s);
                }
                }
            }
            return registers.getOrDefault("z", -1) == 0;
        }

        private Integer registerOrValue(String name) {
            Integer b;
            if ("wxyz".indexOf(name) != -1) {
                b = registers.getOrDefault(name,1 );
            } else {
                b = Integer.parseInt(name);
            }
            return b;
        }
    }

    @Override
    public Program parseInput(List<String> input) {
        Program p = new Program();
        p.instructions = input;
        return p;
    }

    /*
     * <pre>
     * | step | div | check | offset |
     * |    0 |   1 |    11 |      6 |
     * |    1 |   1 |    11 |     12 |
     * |    2 |   1 |    15 |      8 |
     * |    3 |  26 |   -11 |      7 |
     * |    4 |   1 |    15 |      7 |
     * |    5 |   1 |    15 |     12 |
     * |    6 |   1 |    14 |      2 |
     * |    7 |  26 |    -7 |     15 |
     * |    8 |   1 |    12 |      4 |
     * |    9 |  26 |    -6 |      5 |
     * |   10 |  26 |   -10 |     12 |
     * |   11 |  26 |   -15 |     11 |
     * |   12 |  26 |    -9 |     13 |
     * |   13 |  26 |     0 |      7 |
     * 
     * push input[0] + 6               ; push 1
     * push input[1] + 12              ; push 2
     * push input[2] + 8               ; push 3
     * pop input[3] == pop - 11        ; pops 3
     * push input[4] + 7               ; push 4
     * push input[5] + 12              ; push 5
     * push input[6] + 2               ; push 6
     * pop input[7] == pop - 7         ; pops 6
     * push input[8] + 4               ; push 7
     * pop input[9] == input[8] - 6    ; pops 7
     * pop input[10] == input[5] - 10  ; pops 5
     * pop input[11] == input[4] - 15  ; pops 4
     * pop input[12] == input[1] - 9   ; pops 2
     * pop input[12] == input[0] - 0   ; pops 1
     * </pre>
     */

    public long newNumber(long previous, boolean subtract) {
        int[] input = new int[14];

        while (true) {
            if (subtract)
                previous--;
            else
                previous++;
            
            int[] i = Arrays.stream(String.valueOf(previous).split("")).mapToInt(Integer::parseInt).toArray();

            input[0] = i[0];
            input[1] = i[1];
            input[2] = i[2];
            input[4] = i[3];
            input[5] = i[4];
            input[6] = i[5];
            input[8] = i[6];

            input[3] = input[2] + 8 - 11;
            input[7] = input[6] + 2 - 7;
            input[9] = input[8] + 4 - 6;
            input[10] = input[5] + 12 - 10;
            input[11] = input[4] + 7 - 15;
            input[12] = input[1] + 12 - 9;
            input[13] = input[0] + 6 - 0;

            if (Arrays.stream(input).allMatch(x -> x > 0 && x < 10)) {
                String s = Arrays.stream(input).mapToObj(x -> String.valueOf(x)).collect(Collectors.joining());
                return Long.valueOf(s);

            }
        }
    }
    
    @Override
    public Long solver1(Program input) {
        Machine m = new Machine();

        long start = 9999999L;

        
        while (start != 0) {
            start = newNumber(start, true);
            if ( m.isValidSerial(String.valueOf(start), input)) {
                return start;
            }
        }
        return 0L;
    }

    @Override
    public Long solver2(Program input) {
        Machine m = new Machine();

        long start = 1000000L;

        
        while (start != 0) {
            start = newNumber(start, false);
            if ( m.isValidSerial(String.valueOf(start), input)) {
                return start;
            }
        }
        return 0L;
    }
}
