package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day01Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(01);

    Day01 day;

    @Before
    public void before() {
        day = new Day01();
    }

    @Test
    public void part1() {
        assertEquals(66616, (int) day.part1(input.getLines()));
    }

    @Test
    public void part2() {
        assertEquals(199172, (int) day.part2(input.getLines()));
    }

}
