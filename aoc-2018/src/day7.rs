use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{HashMap, HashSet};

#[aoc_generator(day7)]
fn input_generator(input: &str) -> Vec<(String, String)> {
    lazy_static! {
        static ref RE: Regex =
            Regex::new("Step (.+) must be finished before step (.+) can begin.").unwrap();
    }
    input
        .split('\n')
        .map(|l| {
            let caps = RE.captures(l).unwrap();
            (String::from(&caps[1]), String::from(&caps[2]))
        })
        .collect()
}

fn available_entries(entries: &HashMap<&String, HashSet<&String>>) -> Vec<String> {
    let mut open = HashMap::new();

    for k in entries.keys() {
        open.entry(*k).or_insert(false);
        for (_, v) in entries.clone() {
            if v.contains(k) {
                *open.entry(*k).or_insert(false) = true;
            }
        }
    }
    let res: HashMap<&String, bool> = open.into_iter().filter(|(_, v)| !*v).collect();
    Vec::from_iter(res.keys().map(|x| String::from(*x)))
}

type Entries<'a> = HashMap<&'a String, HashSet<&'a String>>;

fn visit(entries: Entries, mut unvisited: Vec<String>) -> (Entries, String, Vec<String>) {
    unvisited.sort_unstable();

    if unvisited.is_empty() {
        panic!("Should not be visiting when empty");
    }

    let mut result = entries.clone();
    // println!("Visiting: {:?} {:?}", unvisited.first(), result);
    let t = unvisited.first().unwrap().to_owned();
    let connected = result.remove(&t).unwrap();
    unvisited.remove(0);
    for c in connected {
        let z = result.values().filter(|x| x.contains(c)).count();
        // println!("Z: {:?} for {}", z, c);
        if z == 0 {
            unvisited.push(String::from(c));
        }
    }
    // println!("Next unvisited: {:?}", unvisited);
    (result, t, unvisited)
}

#[aoc(day7, part1)]
fn part1(input: &[(String, String)]) -> usize {
    // println!("{:?}", input);

    let mut entries = HashMap::new();
    for (key, val) in input {
        entries.entry(key).or_insert(HashSet::new()).insert(val);
    }

    // println!("{:?}", entries);
    let mut answer = String::new();

    let mut more = true;

    let mut available = available_entries(&entries);
    let mut res = entries.clone();
    let mut ans = String::from("");
    // println!("Fist available: {:?}", available);
    while more {
        (res, ans, available) = visit(res, available);
        // println!("{:?} {:?}", res, available);
        if res.is_empty() {
            more = false;
        }
        answer.push(
            ans.chars()
                .collect::<Vec<char>>()
                .first()
                .unwrap()
                .to_owned(),
        );
    }

    println!("Answer: {} ", format!("{}{}", answer, available[0]));
    0
}

#[aoc(day7, part2)]
fn part2(input: &[(String, String)]) -> usize {
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples_part1() {
        let input = input_generator(
            "Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.",
        );
        part1(&input);
    }

    #[test]
    fn examples_part2() {}
}
