package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

/**
 * Day19Test
 */
public class Day19Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(19);

    Day19 day;

    @Before
    public void setup() {
        day = new Day19();
    }


    @Test
    public void sampleOne() {
        List<String> in = new ArrayList<>() {{
                add("     |          ");
                add("     |  +--+    ");
                add("     A  |  C    ");
                add(" F---|----E|--+ ");
                add("     |  |  |  D ");
                add("     +B-+  +--+ ");
            }
        };
        assertEquals("ABCDEF", day.part1(in));
    }

    @Test
    public void partOne() {
        assertEquals("MOABEUCWQS", day.part1(input.getLines()));
    }

    @Test
    public void partTwo() {
        assertEquals("18058", day.part2(input.getLines()));        
    }
}
