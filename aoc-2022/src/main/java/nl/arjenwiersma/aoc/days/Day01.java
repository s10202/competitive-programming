package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import nl.arjenwiersma.aoc.common.NewDay;

public class Day01 extends NewDay<List<List<Integer>>, Integer> {

    @Override
    public List<List<Integer>> parseInput(List<String> input) {
        // One of those things that you first need to go back...
        String orig = input.stream().collect(Collectors.joining("\n"));

        return Arrays.stream(orig.split("\n\n"))
                .map(elf -> Arrays.stream(elf.split("\n")).map(Integer::parseInt).collect(Collectors.toList()))
                .toList();

    }

    @Override
    public Integer solver1(List<List<Integer>> input) {
        return input.stream().map(li -> li.stream().reduce(Integer::sum).get()).max(Integer::compare).get();
    }

    @Override
    public Integer solver2(List<List<Integer>> input) {
        return input.stream().map(li -> li.stream().reduce(0, Integer::sum)) // sum each collection of calories
                .sorted(Collections.reverseOrder()).toList() // reverse sort the list
                .subList(0, 3).stream().reduce(0, Integer::sum); // take 3 and sum them
    }

}
