package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import nl.arjenwiersma.aoc.common.NewDay;

record Operation(int num, int from, int to) {
};

record Operations(List<List<String>> stacks, List<Operation> operations) {
};

public class Day05 extends NewDay<Operations, String> {

    @Override
    public Operations parseInput(List<String> input) {
        String original = input.stream().collect(Collectors.joining("\n"));
        String[] parts = original.split("\n\n");

        String[] stackLines = parts[0].split("\n");
        double size = stackLines[stackLines.length - 1].length();
        int stacks = (int) Math.ceil(size / 4);

        List<List<String>> sts = new ArrayList<>();
        for (int i = 0; i < stacks; i++) {
            sts.add(new ArrayList<>());
        }

        for (int x = 0; x < stackLines.length - 1; x++) {
            String s = stackLines[x];
            for (int i = 0; i < s.length(); i++) {
                switch (s.charAt(i)) {
                case ' ', '[', ']' -> {
                }
                default -> {
                    int stack = (int) Math.ceil((double) i / 4);

                    sts.get(stack - 1).add(0, String.valueOf(s.charAt(i)));
                }
                }
            }
        }

        List<Operation> instructions = new ArrayList<>();
        Operations operations = new Operations(sts, instructions);
        String[] ops = parts[1].split("\n");
        for (String o : ops) {
            String[] steps = o.split(" ");
            instructions.add(
                    new Operation(Integer.parseInt(steps[1]), Integer.parseInt(steps[3]), Integer.parseInt(steps[5])));
        }
        // sts.stream().forEach(x -> {
        // x.stream().forEach(System.out::print);
        // System.out.println();
        // });
        return operations;
    }

    @Override
    public String solver1(Operations input) {

        List<List<String>> stacks = input.stacks();
        for (Operation o : input.operations()) {
            List<String> stack = stacks.get(o.from() - 1);
            List<String> moving = stack.subList(stack.size() - o.num(), stack.size());
            List<String> stackTo = stacks.get(o.to() - 1);
            for (int x = moving.size() - 1; x >= 0; x--) {
                stackTo.add(moving.get(x));
            }

            moving.clear();
            // stacks.stream().forEach(x -> {
            // x.stream().forEach(System.out::print);
            // System.out.println();
            // });
        }

        StringBuilder answer = new StringBuilder();
        for (List<String> stack : stacks) {
            answer.append(stack.get(stack.size() - 1));
        }
        return answer.toString();
    }

    @Override
    public String solver2(Operations input) {
        List<List<String>> stacks = input.stacks();
        for (Operation o : input.operations()) {
            List<String> stack = stacks.get(o.from() - 1);
            List<String> moving = stack.subList(stack.size() - o.num(), stack.size());
            List<String> stackTo = stacks.get(o.to() - 1);

            stackTo.addAll(moving);

            moving.clear();
            // stacks.stream().forEach(x -> {
            // x.stream().forEach(System.out::print);
            // System.out.println();
            // });
        }

        StringBuilder answer = new StringBuilder();
        for (List<String> stack : stacks) {
            answer.append(stack.get(stack.size() - 1));
        }
        return answer.toString();
    }

}
