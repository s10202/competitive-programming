package com.buildfunthings.aoc.days;

import java.util.ArrayList;
import java.util.List;

import com.buildfunthings.aoc.common.Day;

public class Day18 implements Day<Long> {

    public long generateGrid(String current, int iterations) {
        List<String> grid = new ArrayList<>();
        grid.add(current);
        for (int i = 0; i < iterations - 1; i++) {
            StringBuilder newLine = new StringBuilder();
            for (int p = 0; p < current.length(); p++) {
                char[] template = new char[] {
                    (p-1) < 0 ? 46 : current.charAt(p-1),
                    current.charAt(p),
                    (p+1) == current.length() ? 46 : current.charAt(p+1)
                };

                if ((template[0] == '^' && template[1] == '^' && template[2] == '.')
                    ||
                    (template[0] == '.' && template[1] == '^' && template[2] == '^')
                    ||
                    (template[0] == '^' && template[1] == '.' && template[2] == '.')
                    ||
                    (template[0] == '.' && template[1] == '.' && template[2] == '^')
                    ) {
                    newLine.append('^');
                } else {
                    newLine.append('.');                    
                }
            }
                
            current = newLine.toString();
            grid.add(current);
        }

        return grid.stream().flatMap(e -> e.chars().mapToObj(Integer::valueOf)).filter(x -> x == 46).count();
        
    }
    
    @Override
    public Long part1(List<String> input) {
        String current = input.get(0);

        return generateGrid(current, 40);
    }

    @Override
    public Long part2(List<String> input) {
        String current = input.get(0);

        return generateGrid(current, 400000);
    }
    
}
