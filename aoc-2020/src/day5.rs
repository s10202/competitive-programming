#[aoc_generator(day5)]
fn input_generator(input: &str) -> Vec<u16> {
    input
        .split('\n')
        .map(|l| {
            let s: String = l
                .chars()
                .map(|x| match x {
                    'B' => '1',
                    'F' => '0',
                    'R' => '1',
                    'L' => '0',
                    _ => unreachable!(),
                })
                .collect();
            u16::from_str_radix(&s, 2).unwrap()
        })
        .collect()
}

#[aoc(day5, part1)]
fn part1(input: &[u16]) -> u16 {
    *input.iter().max().unwrap() // 978
}

#[aoc(day5, part2)]
fn part2(input: &[u16]) -> u16 {
    let mut data = input.to_owned();
    data.sort_unstable();

    let b = data.windows(2);

    for i in b {
        let l = *i.get(0).unwrap();
        let r = *i.get(1).unwrap();

        if l + 1 < r {
            return l + 1; // 727
        }
    }
    unreachable!()
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_part1() {}

    #[test]
    fn test_part2() {}
}
