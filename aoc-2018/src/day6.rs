use anyhow::anyhow;
use std::{
    collections::{HashMap, HashSet},
    str::FromStr,
};

#[derive(Debug, Copy, Clone)]
struct Point {
    id: i32,
    x: i32,
    y: i32,
}

impl Point {
    // fn new(x: i32, y: i32) -> Point {
    //     Point { id: 0, x, y }
    // }

    fn distance_to(self, x: i32, y: i32) -> i64 {
        ((self.x - x).abs() + (self.y - y).abs()) as i64
    }
}

impl FromStr for Point {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let sp: Vec<&str> = s.split(", ").collect();
        if sp.len() == 2 {
            Ok(Point {
                id: 0,
                x: sp[0].parse().unwrap(),
                y: sp[1].parse().unwrap(),
            })
        } else {
            Err(anyhow!("Incorrect format!"))
        }
    }
}

#[aoc_generator(day6)]
fn input_generator(input: &str) -> Vec<Point> {
    //input.split('\n').filter_map(|l| l.parse().ok()).collect();
    let mut points: Vec<Point> = Vec::new();
    for (index, line) in input.split('\n').into_iter().enumerate() {
        let mut p: Point = line.parse().unwrap();
        p.id = index as i32;
        points.push(p);
    }
    points
}

#[derive(Debug, Clone)]
struct GridPoint {
    id: i32,
    distance: i64,
}

impl GridPoint {
    fn new() -> GridPoint {
        GridPoint {
            id: 0,
            distance: i64::MAX,
        }
    }
    fn new_min() -> GridPoint {
        GridPoint { id: 0, distance: 0 }
    }
}

#[aoc(day6, part1)]
fn part1(input: &[Point]) -> i32 {
    let width = input.iter().max_by(|a, b| a.x.cmp(&b.x)).unwrap().x + 2;
    let height = input.iter().max_by(|a, b| a.y.cmp(&b.y)).unwrap().y + 2;

    // Grid contains a grid point per point containing the
    // point id of the owner and the distance from its owner
    let mut grid_raw = vec![GridPoint::new(); (width * height) as usize];
    let mut grid_base: Vec<_> = grid_raw.as_mut_slice().chunks_mut(width as usize).collect();
    let grid = grid_base.as_mut_slice();

    for point in input {
        for y in 0..height {
            for x in 0..width {
                let mut gp = &mut grid[y as usize][x as usize];
                let dist = gp.distance;
                let calc = point.distance_to(x, y);

                match calc.cmp(&dist) {
                    std::cmp::Ordering::Less => {
                        gp.id = point.id;
                        gp.distance = calc;
                    }
                    std::cmp::Ordering::Equal => gp.id = -1,
                    std::cmp::Ordering::Greater => {}
                }
            }
        }
    }

    let mut infinite = HashSet::new();

    for y in 0..height {
        for x in 0..width {
            let id = grid[y as usize][x as usize].id;
            if id == -1 {
                continue;
            }
            if y == 0 || y == height - 1 || x == 0 || x == width - 1 {
                infinite.insert(id);
            }
        }
    }

    let mut sizes = HashMap::new();
    for y in 0..height {
        for x in 0..width {
            let id = grid[y as usize][x as usize].id;
            if !infinite.contains(&id) {
                *sizes.entry(id).or_insert(0) += 1;
            }
        }
    }

    let res = sizes.iter().max_by(|a, b| a.1.cmp(b.1)).unwrap().1;
    *res // 3401
}

#[aoc(day6, part2)]
fn part2(input: &[Point]) -> usize {
    let width = input.iter().max_by(|a, b| a.x.cmp(&b.x)).unwrap().x + 2;
    let height = input.iter().max_by(|a, b| a.y.cmp(&b.y)).unwrap().y + 2;

    // Grid contains a grid point per point containing the
    // point id of the owner and the distance from its owner
    let mut grid_raw = vec![GridPoint::new_min(); (width * height) as usize];
    let mut grid_base: Vec<_> = grid_raw.as_mut_slice().chunks_mut(width as usize).collect();
    let grid = grid_base.as_mut_slice();

    for y in 0..height {
        for x in 0..width {
            let mut gp = &mut grid[y as usize][x as usize];
            for point in input {
                let calc = point.distance_to(x, y);
                gp.distance += calc;
            }
        }
    }

    let mut infinite = HashSet::new();

    for y in 0..height {
        for x in 0..width {
            let g = &grid[y as usize][x as usize];
            if g.distance < 10000 {
                infinite.insert((x, y));
            }
        }
    }

    infinite.len() // 49327
}

#[cfg(test)]
mod tests {
    use crate::day6::*;

    #[test]
    fn examples_part1() {
        let input = input_generator(
            "1, 1
1, 6
8, 3
3, 4
5, 5
8, 9",
        );
        assert_eq!(part1(&input), 17);
    }

    //     #[test]
    //     fn examples_part2() {
    //         let input = input_generator(
    //             "1, 1
    // 1, 6
    // 8, 3
    // 3, 4
    // 5, 5
    // 8, 9",
    //         );
    //         part2(&input); // 16
    //     }
}
