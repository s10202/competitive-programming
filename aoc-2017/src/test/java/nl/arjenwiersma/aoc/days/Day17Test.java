package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

/**
 * Day17Test
 */
public class Day17Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(17);

    Day17 day;
    
    @Before
    public void before() {
        day = new Day17();
    }

    @Test
    public void sample_one() {
        List<String> input = List.of("3");

        assertEquals(638, (int) day.part1(input));
    }

    @Test
    public void part_one() {
        assertEquals(2000, (int) day.part1(input.getLines()));
    }

    @Test
    public void part_two() {
        assertEquals(10242889, (int) day.part2(input.getLines()));
    }
}
