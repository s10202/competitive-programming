package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import nl.arjenwiersma.aoc.common.Day;
import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day14Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(14);

    Day<Long> day;
    
    @Before
    public void before() {
        day = new Day14();
    }

    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {{
                add("NNCB");
                add("");
                add("CH -> B");
                add("HH -> N");
                add("CB -> H");
                add("NH -> C");
                add("HB -> C");
                add("HC -> B");
                add("HN -> C");
                add("NN -> C");
                add("BH -> H");
                add("NC -> B");
                add("NB -> B");
                add("BN -> B");
                add("BB -> N");
                add("BC -> B");
                add("CC -> N");
                add("CN -> C");
            }};
        assertEquals(1588L, (long)day.part1(input));
        assertEquals(2188189693529L, (long)day.part2(input));
    }

    @Test
    public void part1() {
        assertEquals(5656L, (long)day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(12271437788530L, (long)day.part2(input.getLines()));
    }
}
