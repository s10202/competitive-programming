use std::collections::HashSet;

#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<u32> {
    let mut e: Vec<u32> = input.lines().map(|l| l.parse().unwrap()).collect();

    e.sort_unstable();

    e
}

#[aoc(day1, part1)]
pub fn part1(input: &[u32]) -> u32 {
    let mut h = HashSet::new();
    for (_, v) in input.iter().enumerate() {
        match h.get(&(2020 - v)) {
            Some(r) => {
                return r * v;
            }
            None => h.insert(*v),
        };
    }
    unreachable!()
}

#[aoc(day1, part1, Pointers)]
pub fn part1_pointers(input: &[u32]) -> u32 {
    let target: u32 = 2020;
    let mut left = 0;
    let mut right = input.len() - 1;

    while left < right {
        let current_sum = input[left] + input[right];

        match current_sum.cmp(&target) {
            std::cmp::Ordering::Less => left += 1,
            std::cmp::Ordering::Equal => return input[left] * input[right],
            std::cmp::Ordering::Greater => right -= 1,
        }
    }
    unreachable!()
}

#[aoc(day1, part2)]
pub fn part2(input: &[u32]) -> u32 {
    let target = 2020;

    for i in 0..input.len() - 2 {
        let mut left = i + 1;
        let mut right = input.len() - 1;

        while left < right {
            let current_sum = input[i] + input[left] + input[right];

            match current_sum.cmp(&target) {
                std::cmp::Ordering::Less => left += 1,
                std::cmp::Ordering::Equal => return input[i] * input[left] * input[right],
                std::cmp::Ordering::Greater => right -= 1,
            }
        }
    }

    unreachable!()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&[1721, 979, 366, 299, 675, 1456]), 514579);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&[1721, 979, 366, 299, 675, 1456]), 241861950);
    }
}
