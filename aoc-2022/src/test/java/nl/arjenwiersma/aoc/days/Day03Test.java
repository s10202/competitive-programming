package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day03Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(03);

    Day03 day;

    @Before
    public void before() {
        day = new Day03();
    }

    @Test
    public void part1() {
        String in = """
                vJrwpWtwJgWrhcsFMMfFFhFp
                jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
                PmmdzqPrVvPwwTWBwg
                wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
                ttgJtRGJQctTZtZT
                CrZsJsPPZsGzwwsLwLmpwMDw
                """;
        List<String> test = Arrays.stream(in.split("\n")).toList();

        assertEquals(157, (int) day.part1(test));
        assertEquals(8139, (int) day.part1(input.getLines()));
    }

    @Test
    public void part2() {
        String in = """
                vJrwpWtwJgWrhcsFMMfFFhFp
                jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
                PmmdzqPrVvPwwTWBwg
                wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
                ttgJtRGJQctTZtZT
                CrZsJsPPZsGzwwsLwLmpwMDw
                """;
        List<String> test = Arrays.stream(in.split("\n")).toList();

        assertEquals(70, (int) day.part2(test));
        assertEquals(2668, (int) day.part2(input.getLines()));
    }

}
