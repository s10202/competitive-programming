package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

/**
 * Day21Test
 */
public class Day21Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(21);

    Day21 day;

    @Before
    public void before() {
        day = new Day21();
    }

    @Test
    public void testSampleOne() {
        List<String> in = new ArrayList<>() {{
                add("../.# => ##./#../...");
                add(".#./..#/### => #..#/..../..../#..# ");
            }};

        day.part1(in);
    }
}
