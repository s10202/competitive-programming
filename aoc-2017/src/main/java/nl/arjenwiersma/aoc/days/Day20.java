package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.arjenwiersma.aoc.common.Day;

/**
 * Day20
 */
public class Day20 extends Day<List<Day20.Particle>, Integer> {

    public class Point3D {
        long x = 0;
        long y = 0;
        long z = 0;

        public Point3D(long x, long y, long z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public void add(Point3D other) {
            this.x += other.x;
            this.y += other.y;
            this.z += other.z;
        }

        @Override
        public String toString() {
            return "Point3D [x=" + x + ", y=" + y + ", z=" + z + "]";
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getEnclosingInstance().hashCode();
            result = prime * result + (int) (x ^ (x >>> 32));
            result = prime * result + (int) (y ^ (y >>> 32));
            result = prime * result + (int) (z ^ (z >>> 32));
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Point3D other = (Point3D) obj;
            if (!getEnclosingInstance().equals(other.getEnclosingInstance()))
                return false;
            if (x != other.x)
                return false;
            if (y != other.y)
                return false;
            if (z != other.z)
                return false;
            return true;
        }

        private Day20 getEnclosingInstance() {
            return Day20.this;
        }

    }
    
    public class Particle {
        int id;
        Point3D position;
        Point3D velocity;
        Point3D acceleration;

        public Particle(int id, Point3D position, Point3D velocity, Point3D acceleration) {
            this.id = id;
            this.position = position;
            this.velocity = velocity;
            this.acceleration = acceleration;
        }

        public long distance() {
            return Math.abs(position.x) + Math.abs(position.y) + Math.abs(position.z);
        }

        @Override
        public String toString() {
            return "Particle [id=" + id + ", position=" + position + ", acceleration=" + acceleration + ",  velocity=" + velocity + "]";
        }

        public Point3D getPosition() {
            return position;
        }

        public void setPosition(Point3D position) {
            this.position = position;
        }

    }

    @Override
    public List<Particle> parseInput(List<String> input) {
        // p=< 3,0,0>, v=< 2,0,0>, a=<-1,0,0>
        Pattern p = Pattern.compile("<([\\s-\\d,]+)>, [a-z]=<([\\s-\\d,]+)>, [a-z]=<([\\s-\\d,]+)");
        List<Particle> particles = new ArrayList<>();

        int n = 0;
        for (String in : input) {
            Matcher m = p.matcher(in);
            if (m.find()) {
                Point3D[] pts = new Point3D[3];

                for (int i = 0; i < 3; i++) {
                    int[] d = Arrays.stream(m.group(i + 1).split(",")).mapToInt(x -> Integer.parseInt(x.trim())).toArray();
                    pts[i] = new Point3D(d[0], d[1], d[2]);
                }
                particles.add( new Particle(n, pts[0], pts[1], pts[2]) );
            } else {
                System.out.println("Input was not parsable: " + in);
            }
            n++;
        }
        
        return particles;
    }

    private int closestAfterMove(List<Particle> particles) {
        return particles.stream().map(this::step)
            .min(Comparator.comparing(Particle::distance)).map(x -> x.id).orElse(0);
    }

    
    public Particle step(Particle original) {
        original.velocity.add(original.acceleration);
        original.position.add(original.velocity);
        return original;
    }

    
    @Override
    public Integer solver1(List<Particle> input) {
        int id = 0;
        for (int i = 0; i < 400; i++) {
            id = closestAfterMove(input);
        }

        return id;
    }

    private List<Particle> boom(List<Particle> particles) {
        // group by position
        // only keep the ones that have size 1
        Map<Point3D, Integer> seen = new HashMap<>();
        for (Particle p : particles) {
            seen.merge(p.position, 1, Integer::sum);
        }

        return particles.stream().filter(x -> seen.getOrDefault(x.position, 1) == 1).toList();
    }
    
    @Override
    public Integer solver2(List<Particle> input) {
        for (int i = 0; i < 100; i++) {
            closestAfterMove(input);
            input = boom(input);
        }

        return input.size();
    }
    
    
}
