package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day12StreamTest {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(12);

    Day12Stream day;
    
    @Before
    public void before() {
        day = new Day12Stream();
    }

    @Test
    public void testSampleSmall() {
        List<String> input = new ArrayList<>() {{
                add("start-A");
                add("start-b");
                add("A-c");
                add("A-b");
                add("b-d");
                add("A-end");
                add("b-end");
            }};
        assertEquals(36, (int) day.part2(input));
    }
    
    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {{
                add("dc-end");
                add("HN-start");
                add("start-kj");
                add("dc-start");
                add("dc-HN");
                add("LN-dc");
                add("HN-end");
                add("kj-sa");
                add("kj-HN");
                add("kj-dc");
            }};
        assertEquals(19, (int) day.part1(input));
        assertEquals(103, (int) day.part2(input));

        input = new ArrayList<>() {{
                 add("fs-end");
                 add("he-DX");
                 add("fs-he");
                 add("start-DX");
                 add("pj-DX");
                 add("end-zg");
                 add("zg-sl");
                 add("zg-pj");
                 add("pj-he");
                 add("RW-he");
                 add("fs-DX");
                 add("pj-RW");
                 add("zg-RW");
                 add("start-pj");
                 add("he-WI");
                 add("zg-he");
                 add("pj-fs");
                 add("start-RW");
            }};
        assertEquals(226, (int) day.part1(input));
        assertEquals(3509, (int) day.part2(input));
    }

    @Test
    public void part1() {
        assertEquals(3576, (int) day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(84271, (int) day.part2(input.getLines()));
    }
}
