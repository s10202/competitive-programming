package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day18Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(18);

    Day18 day;
    
    @Before
    public void before() {
        day = new Day18();
    }

    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {{
                add("[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]");
                add("[[[5,[2,8]],4],[5,[[9,9],0]]]");
                add("[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]");
                add("[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]");
                add("[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]");
                add("[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]");
                add("[[[[5,4],[7,7]],8],[[8,3],8]]");
                add("[[9,3],[[9,9],[6,[4,9]]]]");
                add("[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]");
                add("[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]");
            }};

        assertEquals(4140L, (long) day.part1(input));
        assertEquals(3993L, (long) day.part2(input));
    }

    @Test
    public void part1() {
        assertEquals(3654, (long) day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(4578, (long) day.part2(input.getLines()));
    }
}
