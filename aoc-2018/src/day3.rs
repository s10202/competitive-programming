use std::{cmp, collections::HashMap, str::FromStr};

use regex::Regex;

#[derive(Debug, Hash, PartialEq, Eq)]
struct Claim {
    id: u32,
    x: u32,
    y: u32,
    w: u32,
    h: u32,
}

impl FromStr for Claim {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").unwrap();
        let cap = re.captures(s).unwrap();
        if cap.len() == 6 {
            Ok(Claim {
                id: cap[1].parse().unwrap(),
                x: cap[2].parse().unwrap(),
                y: cap[3].parse().unwrap(),
                w: cap[4].parse().unwrap(),
                h: cap[5].parse().unwrap(),
            })
        } else {
            Err(anyhow::format_err!("Wrong format!"))
        }
    }
}

#[derive(Hash, Debug, PartialEq, Eq)]
struct Point {
    x: u32,
    y: u32,
}

#[aoc_generator(day3)]
fn input_generator(input: &str) -> Vec<Claim> {
    //                           #1247 @ 436,777: 27x25
    input
        .split('\n')
        .filter_map(|l| l.parse::<Claim>().ok())
        .collect()
}

#[aoc(day3, part1)]
fn part1(input: &[Claim]) -> usize {
    let mut grid: HashMap<Point, u32> = HashMap::new();
    for claim in input {
        for y in claim.y..claim.y + claim.h {
            for x in claim.x..claim.x + claim.w {
                *grid.entry(Point { x, y }).or_insert(0) += 1;
            }
        }
    }
    grid.iter().filter(|(_key, val)| **val > 1).count() // 100261
}

#[aoc(day3, part2)]
fn part2(input: &[Claim]) -> u32 {
    let mut grid = HashMap::new();
    for claim in input {
        for y in claim.y..claim.y + claim.h {
            for x in claim.x..claim.x + claim.w {
                grid.entry(Point { x, y }).or_insert_with(Vec::new);
                grid.get_mut(&Point { x, y }).unwrap().push(claim);
            }
        }
    }
    //println!("{:?}", grid);
    let mut seen = HashMap::new();
    for point in grid {
        let size: u32 = point.1.len() as u32;
        for c in point.1 {
            if !seen.contains_key(c) {
                seen.entry(c).or_insert(0);
            }
            let cur = cmp::max(seen.get(c).unwrap().to_owned(), size);
            *seen.entry(c).or_insert(0) = cur;
        }
    }

    for claim in seen {
        if claim.1 == 1 {
            return claim.0.id; // 251
        }
    }
    0
}

#[cfg(test)]
mod tests {
    use crate::day3::*;

    #[test]
    fn parsing_input() {
        let res = input_generator(
            "#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2",
        );

        assert_eq!(res.len(), 3);
        let c = res.get(0).unwrap();
        assert_eq!(c.id, 1);
        assert_eq!(c.x, 1);
        assert_eq!(c.y, 3);
        assert_eq!(c.w, 4);
        assert_eq!(c.h, 4);
    }

    #[test]
    fn examples_part1() {
        let res = input_generator(
            "#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2",
        );

        assert_eq!(part1(&res), 4);
    }

    #[test]
    fn examples_part2() {
        let res = input_generator(
            "#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2",
        );

        assert_eq!(part2(&res), 3);
    }
}
