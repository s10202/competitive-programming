package com.buildfunthings.aoc.utils;

public class ArrayUtils {

    public static void leftRotate(int arr[], int d, int n)
    {
        // To handle if d >= n
        d = d % n;
        int i, j, k, temp;
        int g_c_d =  gcd(d, n);
 
        for (i = 0; i < g_c_d; i++) {
 
            // move i-th values of blocks
            temp = arr[i];
            j = i;
 
            // Performing sets of operations if
            // condition holds true
            while (true) {
                k = j + d;
                if (k >= n)
                    k = k - n;
                if (k == i)
                    break;
                arr[j] = arr[k];
                j = k;
            }
            arr[j] = temp;
        }
    }

    public static void rightRotate(int arr[], int d, int n)
    {
 
        // To use as left rotation
        d = n - d;
        d = d % n;
        int i, j, k, temp;
        int g_c_d = gcd(d, n);
 
        for (i = 0; i < g_c_d; i++) {
 
            // Moving i-th values of blocks
            temp = arr[i];
            j = i;
 
            while (true) {
                k = j + d;
                if (k >= n)
                    k = k - n;
                if (k == i)
                    break;
                arr[j] = arr[k];
                j = k;
            }
            arr[j] = temp;
        }
    }    

    public static int gcd(int a, int b)
    {
        if (b == 0)
            return a;
        else
            return gcd(b, a % b);
    }
    
    public static <T> T[] rotate(T[] array, int destPos) {
        T[] tmp = (T[]) new Object[array.length];
        System.arraycopy(array, array.length - destPos, tmp, 0, destPos);
        System.arraycopy(array, 0, array, destPos, array.length - destPos);
        System.arraycopy(tmp, 0, array, 0, destPos);
        return tmp;
    }
}
