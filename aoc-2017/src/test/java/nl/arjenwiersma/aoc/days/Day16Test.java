package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

/**
 * Day16Test
 */
public class Day16Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(16);

    Day16 day;
    
    @Before
    public void before() {
        day = new Day16();
    }

    @Test
    public void testOps() {
        assertEquals("eabcd", day.spin("abcde", 1));
        assertEquals("cdeab", day.spin("abcde", 3));

        assertEquals("baedc", day.partner("eabdc", new String[] { "e", "b" }));
        assertEquals("cabde", day.partner("eabdc", new String[] { "e", "c" }));
        assertEquals("eadbc", day.partner("eabdc", new String[] { "d", "b" }));
    }
    
    @Test
    public void testSample() {
        day.setPrograms("abcde");

        List<String> input = new ArrayList<>() {{
                add("s1,x3/4,pe/b");
            }
        };

        assertEquals("baedc", day.part1(input));
    }

    @Test
    public void part_one() {
        assertEquals("kbednhopmfcjilag", day.part1(input.getLines()));
    }
        
    @Test
    public void part_two() {
        assertEquals("fbmcgdnjakpioelh", day.part2(input.getLines()));
    }
   
}
