package com.buildfunthings.aoc.days;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.buildfunthings.aoc.common.Day;

public class Day19 implements Day<Integer> {

    @Override
    public Integer part1(List<String> input) {
        int elves = Integer.parseInt(input.get(0));

        // https://www.youtube.com/watch?v=uCsD3ZGzMgE - Josephs Problem
        String binary = Integer.toBinaryString(elves);
        return Integer.parseInt(binary.substring(1) + binary.charAt(0), 2);
        
        // System.out.println(binary + " " + result);
        // Deque<Integer> queue = IntStream.rangeClosed(1,elves).boxed().collect(Collectors.toCollection(ArrayDeque::new));

        // while (queue.size() > 1) {
        //     queue.addLast(queue.pop()); // add the stealer to the back
        //     queue.pop(); // the one stolen from
        // }

        // return queue.pop();
    }

    @Override
    public Integer part2(List<String> input) {
        int elves = Integer.parseInt(input.get(0));
        
        Deque<Integer> leftSide = IntStream.rangeClosed(1, elves / 2).boxed().collect(Collectors.toCollection(ArrayDeque::new));
        Deque<Integer> rightSide = IntStream.rangeClosed(elves / 2 + 1, elves).boxed().collect(Collectors.toCollection(ArrayDeque::new));

        while (leftSide.size() + rightSide.size() > 1) { // while there is one to take
            rightSide.poll();                            // take from right
            rightSide.add(leftSide.poll());              // add the top from left to right
            
            // System.out.println(leftSide);
            // System.out.println(rightSide);
            // System.out.println();
            if ((leftSide.size() + rightSide.size()) % 2 == 0) // if we have even numbers
                leftSide.add(rightSide.poll());                // rebalance from left to right
            // System.out.println(leftSide);
            // System.out.println(rightSide);
            // System.out.println();
        }
        return rightSide.poll();
    }

}
