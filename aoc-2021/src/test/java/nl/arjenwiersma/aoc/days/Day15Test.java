package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.Day;
import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day15Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(15);

    Day<Long> day;
    
    @Before
    public void before() {
        day = new Day15();
    }

    @Test
    public void testSample() {
        List<String> input = new ArrayList<>() {{
                add("1163751742");
                add("1381373672");
                add("2136511328");
                add("3694931569");
                add("7463417111");
                add("1319128137");
                add("1359912421");
                add("3125421639");
                add("1293138521");
                add("2311944581");
            }
        };
        assertEquals(40L, (long)day.part1(input));
        assertEquals(315L, (long)day.part2(input));
    }

    @Test
    public void part1() {
        assertEquals(527L, (long)day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(2887L, (long)day.part2(input.getLines()));
    }
}
