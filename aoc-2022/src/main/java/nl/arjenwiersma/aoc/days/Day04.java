package nl.arjenwiersma.aoc.days;

import java.util.List;

import nl.arjenwiersma.aoc.common.NewDay;

record Pairs(int f_start, int f_end, int s_start, int s_end) {
};

public class Day04 extends NewDay<List<Pairs>, Integer> {

    @Override
    public List<Pairs> parseInput(List<String> input) {
        return input.stream().map(l -> {
            String[] pairs = l.split(",");
            String[] first = pairs[0].split("-");
            String[] second = pairs[1].split("-");
            return new Pairs(Integer.parseInt(first[0]), Integer.parseInt(first[1]), Integer.parseInt(second[0]),
                    Integer.parseInt(second[1]));
        }).toList();
    }

    @Override
    public Integer solver1(List<Pairs> input) {
        int overlap = 0;
        for (Pairs p : input) {
            if ((p.f_start() <= p.s_start() && p.f_end() >= p.s_end())
                    || (p.s_start() <= p.f_start() && p.s_end() >= p.f_end())) {
                overlap++;
            }
        }
        return overlap;
    }

    @Override
    public Integer solver2(List<Pairs> input) {
        int overlap = 0;
        for (Pairs p : input) {
            if ((p.s_start() >= p.f_start() && p.s_start() <= p.f_end())
                    || (p.f_start() >= p.s_start() && p.f_start() <= p.s_end())) {
                overlap++;
            }
        }
        return overlap;
    }

}
