package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day04Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(04);

    Day04 day;

    @Before
    public void before() {
        day = new Day04();
    }

    @Test
    public void part1() {
        String in = """
                2-4,6-8
                2-3,4-5
                5-7,7-9
                2-8,3-7
                6-6,4-6
                2-6,4-8
                """;
        List<String> test = Arrays.stream(in.split("\n")).toList();

        assertEquals(2, (int) day.part1(test));
        assertEquals(471, (int) day.part1(input.getLines()));
    }

    @Test
    public void part2() {
        String in = """
                2-4,6-8
                2-3,4-5
                5-7,7-9
                2-8,3-7
                6-6,4-6
                2-6,4-8
                """;
        List<String> test = Arrays.stream(in.split("\n")).toList();

        assertEquals(4, (int) day.part2(test));
        assertEquals(888, (int) day.part2(input.getLines()));
    }

}
