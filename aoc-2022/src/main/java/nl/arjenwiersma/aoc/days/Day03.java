package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import nl.arjenwiersma.aoc.common.NewDay;

record Compartment(String left, String right) {
};

public class Day03 extends NewDay<List<Compartment>, Integer> {

    @Override
    public List<Compartment> parseInput(List<String> input) {
        return input.stream().map(l -> new Compartment(l.substring(0, l.length() / 2), l.substring(l.length() / 2)))
                .toList();
    }

    @Override
    public Integer solver1(List<Compartment> input) {
        int sum = 0;
        for (Compartment c : input) {
            Set<String> left = Arrays.stream(c.left().split("")).collect(Collectors.toSet());
            Set<String> right = Arrays.stream(c.right().split("")).collect(Collectors.toSet());
            left.retainAll(right);

            assert (left.size() == 1);

            for (String single : left) { // will only be 1
                sum += priority(single);
            }
        }
        return sum;
    }

    private int priority(String single) {
        int v = single.charAt(0) - 'a' + 1;
        if (v < 0) {
            v = single.charAt(0) - 'A' + 27;
        }
        return v;
    }

    private String commonInAll(List<Compartment> rugsack) {
        List<Set<String>> sets = new ArrayList<>();
        for (Compartment c : rugsack) {
            sets.add(Arrays.stream((c.left() + c.right()).split("")).collect(Collectors.toSet()));
        }

        sets.get(0).retainAll(sets.get(1));
        sets.get(0).retainAll(sets.get(2));

        assert (sets.get(0).size() == 1);
        return sets.get(0).stream().collect(Collectors.joining());
    }

    @Override
    public Integer solver2(List<Compartment> input) {
        List<List<Compartment>> groups = Lists.partition(input, 3);

        int sum = 0;
        for (List<Compartment> g : groups) {
            sum += priority(commonInAll(g));
        }
        return sum;
    }

}
