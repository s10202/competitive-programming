package com.buildfunthings.aoc.days;

import java.util.Arrays;
import java.util.List;

import com.buildfunthings.aoc.common.Day;

public class Day21 implements Day<String> {

    void leftRotate(char arr[], int steps) {
        char temp[] = new char[steps];

        for (int i = 0; i < steps; i++)
            temp[i] = arr[i];

        for (int i = steps; i < arr.length; i++) {
            arr[i - steps] = arr[i];
        }

        for (int i = 0; i < steps; i++) {
            arr[i + arr.length - steps] = temp[i];
        }
    }

    void rightRotate(char arr[], int steps) {
        int n = arr.length;
        while (steps > n) {
            steps = steps - n;
        }

        char temp[] = new char[n - steps];

        for (int i = 0; i < n - steps; i++)
            temp[i] = arr[i];

        for (int i = n - steps; i < n; i++) {
            arr[i - n + steps] = arr[i];
        }

        for (int i = 0; i < n - steps; i++) {
            arr[i + steps] = temp[i];
        }
    }

    public void rotateOnChar(char[] arr, char x) {
        int posx = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == x)
                posx = i;
        }

        rightRotate(arr, 1 + posx + (posx >= 4 ? 1 : 0));
    }
    // 3 rightRotate 4
    // 5 rightRotate 7
    // 7 
    public void rotateOnCharReverse(char[] arr, char x) {
        int posx = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == x)
                posx = i;
        }

        // oldpos shift  posx
        //   0     1      1
        //   1     2      3
        //   2     3      5
        //   3     4      7
        //   4     6      2
        //   5     7      4
        //   6     8      6
        //   7     9      0
        int rot = posx / 2 + ((posx % 2 == 1 || posx == 0) ? 1 : 5); 
        leftRotate(arr, rot);
    }

    public void reverseSection(char[] arr, int start, int end) {
        char[] tmp = new char[arr.length];

        for (int i = 0; i < arr.length; i++)
            tmp[i] = arr[i];

        for (int i = 0; i <= (end - start); i++) {
            arr[start + i] = tmp[end - i];
        }
    }

    public void swap(char[] arr, int x, int y) {
        char c = arr[x];
        arr[x] = arr[y];
        arr[y] = c;
    }

    public void swap(char[] arr, char x, char y) {
        int posx = 0;
        int posy = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == x)
                posx = i;
            if (arr[i] == y)
                posy = i;
        }

        swap(arr, posx, posy);
    }

    public void move(char[] arr, int posx, int toy) {

        char tmp = arr[posx];
        //System.out.println(tmp);
        // [ 0, 1, 2, 3, 4, 5 ]
        // [ 0, 1, 3, 4, 2, 5 ]
        if (toy > posx) {
            for (int i = 0; i < arr.length; i++) {
                if (i >= posx && i < toy) {
                    arr[i] = arr[i + 1];
                } else if (i == toy) {
                    arr[i] = tmp;
                } else {
                    arr[i] = arr[i];
                }

            }
        } else {
            for (int i = arr.length - 1; i >= 0; i--) {
                if (i <= posx && i > toy) {
                    arr[i] = arr[i - 1];
                } else if (i == toy) {
                    arr[i] = tmp;
                } else {
                    arr[i] = arr[i];
                }
            }
        }
    }

    @Override
    public String part1(List<String> input) {
        // char[] pass = new char[] { 'a', 'b', 'c', 'd', 'e' };

        // swap(pass, 4, 0);
        // swap(pass, 'd', 'b');
        // reverseSection(pass, 0, 4);
        // leftRotate(pass, 1);
        // move(pass, 1, 4);
        // move(pass, 3, 0);
        // rotateOnChar(pass, 'b');
        // rotateOnChar(pass, 'd');

        // System.out.println(pass);

        char[] pass = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };

        for (String in : input) {
            String[] parts = in.trim().split(" ");
            switch (parts[0]) {
            case "swap" -> {
                switch (parts[1]) {
                case "position" -> swap(pass, Integer.parseInt(parts[2]), Integer.parseInt(parts[5]));
                case "letter" -> swap(pass, parts[2].charAt(0), parts[5].charAt(0));
                }
            }
            case "rotate" -> {
                switch (parts[1]) {
                case "left" -> leftRotate(pass, Integer.parseInt(parts[2]));
                case "right" -> rightRotate(pass, Integer.parseInt(parts[2]));
                case "based" -> rotateOnChar(pass, parts[6].charAt(0));
                }
            }
            case "reverse" -> reverseSection(pass, Integer.parseInt(parts[2]), Integer.parseInt(parts[4]));
            case "move" -> move(pass, Integer.parseInt(parts[2]), Integer.parseInt(parts[5]));
            }
        }
        
        StringBuilder sb = new StringBuilder();
        for (char c : pass) {
            sb.append(c);
        }
        return sb.toString();
    }

    @Override
    public String part2(List<String> input) {
        char[] pass = new char[] { 'f', 'b', 'g', 'd', 'c', 'e', 'a', 'h' };

        for (int i = input.size() - 1; i >= 0 ; i--) {
            String in = input.get(i);
            String[] parts = in.trim().split(" ");
            switch (parts[0]) {
            case "swap" -> {
                switch (parts[1]) {
                case "position" -> swap(pass, Integer.parseInt(parts[5]), Integer.parseInt(parts[2]));
                case "letter" -> swap(pass, parts[5].charAt(0), parts[2].charAt(0));
                }
            }
            case "rotate" -> {
                switch (parts[1]) {
                case "left" -> rightRotate(pass, Integer.parseInt(parts[2]));
                case "right" -> leftRotate(pass, Integer.parseInt(parts[2]));
                case "based" -> rotateOnCharReverse(pass, parts[6].charAt(0));
                }
            }
            case "reverse" -> reverseSection(pass, Integer.parseInt(parts[2]), Integer.parseInt(parts[4]));
            case "move" -> move(pass, Integer.parseInt(parts[5]), Integer.parseInt(parts[2]));
            }
        }
        
        StringBuilder sb = new StringBuilder();
        for (char c : pass) {
            sb.append(c);
        }
        return sb.toString();
    }

}
