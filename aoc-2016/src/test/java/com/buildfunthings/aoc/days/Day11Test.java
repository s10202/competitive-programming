package com.buildfunthings.aoc.days;

import static org.junit.Assert.assertEquals;

import com.buildfunthings.aoc.common.Day;
import com.buildfunthings.aoc.common.DayInputExternalResource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class Day11Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(11);
    private Day<Integer> day;

    @Before
    public void setupTest() {
        day = new Day11();
    }
    
    @Test
    public void part1() {
        assertEquals(Integer.valueOf(0), day.part1(input.getLines())); // 37
    }
    
    @Test
    public void part2() {
        assertEquals(Integer.valueOf(0), day.part2(input.getLines())); // 61
    }
}
