package nl.arjenwiersma.aoc.days;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import nl.arjenwiersma.aoc.common.NewDay;

public class Day21 extends NewDay<int[], Long> {

    @Override
    public int[] parseInput(List<String> input) {
        int[] players = new int[input.size()];
        for (String in : input) {
            String[] parts = in.split(" ");
            players[Integer.parseInt(parts[1]) - 1] = Integer.parseInt(parts[4]);
        }

        return players;
    }

    @Override
    public Long solver1(int[] input) {
        // roll die 3 times
        boolean winner = false;
        int i = 1;
        int[] scores = new int[input.length];

        int player = 0;
        long rolls = 0;
        while (!winner) {
            int roll = i + i + 1 + i + 2;
            rolls += 3;

            int score = (input[player] + roll) % 10;

            scores[player] += (score == 0 ? 10 : score);
            input[player] = (score == 0 ? 10 : score);

            player = (player == 0 ? 1 : 0);

            i += 3;

            winner = Arrays.stream(scores).anyMatch(x -> x >= 1000);
        }
        System.out.println("Winner: " + Arrays.toString(scores) + " after " + rolls);
        return rolls * Arrays.stream(scores).min().getAsInt();
    }

    public Map<Integer, Long> play(GameState state, Map<GameState, Map<Integer, Long>> memo) {
        if (memo.containsKey(state)) {
            return memo.get(state);
        }

        Map<Integer, Long> wins = new HashMap<>() {
            {
                put(1, 0L);
                put(2, 0L);
            }
        };

        if (state.rolls > 0) {
            for (int die = 1; die <= 3; die++) {
                addWins(wins, play(new GameState(state.p1_score, state.p2_score, state.p1_pos, state.p2_pos,
                        state.player, state.sum + die, state.rolls - 1), memo));
            }
        } else {
            if (state.player == 1) {
                int posres = state.p1_pos + state.sum;
                int newpos = (posres % 10 == 0 ? 10 : (posres % 10));
                long score = state.p1_score + newpos;

                if (score >= 21) { // win condition
                    addWin(wins, 1, 1L);
                } else {
                    // turn ended, other player
                    addWins(wins, play(new GameState(score, state.p2_score, newpos, state.p2_pos, 2, 0, 3), memo));
                }
            } else {
                int posres = state.p2_pos + state.sum;
                int newpos = (posres % 10 == 0 ? 10 : (posres % 10));
                long score = state.p2_score + newpos;

                if (score >= 21) { // win condition
                    addWin(wins, 2, 1L);
                } else {
                    // turn ended, other player
                    addWins(wins, play(new GameState(state.p1_score, score, state.p1_pos, newpos, 1, 0, 3), memo));
                }
                
            }
        }

        memo.put(state, wins);

        return wins;
    }

    public void addWin(Map<Integer, Long> previous, int player, long score) {
        previous.put(player, previous.get(player)+score);
    }

    public void addWins(Map<Integer, Long> previous, Map<Integer, Long> current) {
        for(int k : current.keySet()) {
            previous.put(k, previous.get(k) + current.get(k));            
        }
    }

    record GameState(long p1_score, long p2_score, int p1_pos, int p2_pos, int player, int sum, int rolls) {

    }

    @Override
    public Long solver2(int[] input) {
        GameState gs = new GameState(0, 0, input[0], input[1], 1, 0, 3);

        // gamestate with win mapping
        Map<GameState, Map<Integer, Long>> memo = new HashMap<>();
        // win states
        Map<Integer, Long> result = play(gs, memo);
        
        return Math.max(result.get(1), result.get(2));
    }

}
