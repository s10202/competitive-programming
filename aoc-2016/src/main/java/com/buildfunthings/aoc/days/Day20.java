package com.buildfunthings.aoc.days;

import java.util.ArrayList;
import java.util.List;

import com.buildfunthings.aoc.common.Day;

public class Day20 implements Day<Long> {

    @Override
    public Long part1(List<String> input) {
        return getAllowedIp(input, true);        
    }

    @Override
    public Long part2(List<String> input) {
        return getAllowedIp(input, false);        
    }

    private Long getAllowedIp(List<String> input, boolean returnFirstFree) {
        List<Long[]> entries = new ArrayList<>();
        for (String in : input) {
            String[] parts = in.trim().split("-");
            Long start = Long.parseLong(parts[0]);
            Long end = Long.parseLong(parts[1]);

            entries.add(new Long[] { start, end });
        }
        entries.sort((a,b) -> Long.compare(a[0], b[0]));

        Long[] lastOne = entries.get(0);
        Long howMany = lastOne[0] > 0? lastOne[0] : 0;
        System.out.println("Starting with " + lastOne[0]);
        for (int i = 1; i < entries.size(); i++) {
            Long[] currentOne = entries.get(i);

            // ensure there is a gap between the current start and the last end position
            if (currentOne[0] > ( lastOne[1] + 1 )) {
                if (returnFirstFree)
                    return lastOne[1] + 1;
                howMany += (currentOne[0] - (lastOne[1] + 1));
            }

            // only pick the current one if the last one does not fully overlap it
            if (lastOne[1] < currentOne[1])
                lastOne = currentOne;
        }
        if (lastOne[1] < 4294967295L) {
            howMany += 4294967295L - lastOne[1];
        }
        return howMany;
    }
    
}
