package nl.arjenwiersma.aoc.utils;

import java.util.Arrays;

public class GridUtils {
    /**
     * print a grid of a 2d int
     */
    public static void printGrid(int[][] field) {
        printGrid(field, "%d");
    }
    
    /**
     * print a grid of a 2d int array, using the specified format
     */
    public static void printGrid(int[][] field, String format) {
        Arrays.stream(field).forEach(x -> {
                Arrays.stream(x).forEach(z -> System.out.printf(format, z));
                System.out.println();
        });
    }
    
}
