package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.Day;
import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day16Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(16);

    Day<Long> day;
    
    @Before
    public void before() {
        day = new Day16();
    }

    @Test
    public void testSample() {
        Map<String, Long> tests = new HashMap<>() {{
                // put("D2FE28");
                // put("38006F45291200");
                // put("EE00D40C823060");
                put("8A004A801A8002F478",16L);
                put("620080001611562C8802118E34",12L);
                put("C0015000016115A2E0802F182340",23L);
                put("A0016C880162017C3686B18A3D4780",31L);
                }
            };

        for (String s : tests.keySet()) {
            assertEquals(tests.get(s), day.part1(new ArrayList<>() {
                {
                    add(s);
                }}));
        }
    }

    @Test
    public void testSample2() {
        Map<String, Long> tests = new HashMap<>() {{
                put("C200B40A82", 3L);
                put("04005AC33890", 54L);
                put("880086C3E88112", 7L);
                put("CE00C43D881120", 9L);
                put("D8005AC2A8F0", 1L);
                put("F600BC2D8F", 0L);
                put("9C005AC2F8F0", 0L);
                put("9C0141080250320F1802104A08", 1L);

            }
            };

        for (String s : tests.keySet()) {
            assertEquals(tests.get(s), day.part2(new ArrayList<>() {
                {
                    add(s);
                }}));
        }
        
    }
    
    @Test
    public void part1() {
        assertEquals(875L, (long)day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(1264857437203L, (long) day.part2(input.getLines()));
    }
}
