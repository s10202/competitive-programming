package nl.arjenwiersma.aoc.days;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.arjenwiersma.aoc.common.NewDay;

public class Day12Stream extends NewDay<Map<String, List<String>>, Integer> {

    @Override
    public Map<String, List<String>> parseInput(List<String> input) {
        Map<String, List<String>> connections = new HashMap<>();

        for (String in : input) {
            String[] parts = in.split("-");
            String from = parts[0].trim();
            String to = parts[1].trim();

            addConnection(connections, from, to);
            addConnection(connections, to, from);
        }

        return connections;
    }

    private void addConnection(Map<String, List<String>> connections, String from, String to) {
        List<String> others = connections.getOrDefault(from, new ArrayList<>());
        others.add(to);
        connections.put(from, others);
    }

    private int findPaths(Map<String, List<String>> input, boolean allowPairs) {
        Deque<List<String>> q = new ArrayDeque<>();

        List<String> start = new ArrayList<>() {
            {
                add("start");
            }
        };
        q.add(start);

        List<List<String>> paths = new ArrayList<>();
        while (!q.isEmpty()) {
            List<String> path = q.pop();

            List<String> caves = input.get(path.get(path.size() - 1));

            for (String cave : caves) {
                if ("start".equals(cave))
                    continue;

                if (Character.isLowerCase(cave.charAt(0)) && path.contains(cave)) {
                    if (!allowPairs)
                        continue;

                    Map<String, Integer> freqs = new HashMap<>();
                    path.stream().filter(s -> Character.isLowerCase(s.charAt(0)))
                            .forEach(c -> freqs.merge(c, 1, Integer::sum));

                    boolean hasPair = freqs.values().stream().anyMatch(x -> x == 2);

                    if (hasPair) 
                        continue;                    
                }

                List<String> newpath = new ArrayList<>(path);
                newpath.add(cave);

                if ("end".equals(cave)) {
                    paths.add(newpath);
                } else {
                    q.add(newpath);
                }
            }
        }
        return paths.size();
    }

    // accumulator -> bijhouden resultaten
    private int recursePath(Map<String, List<String>> connecties, List<String> path) {
        String last = path.get(path.size() - 1);
        if ("end".equals(last))
            return 1;

        int solutions = 0;
        for (String cave : connecties.get(last)) {
            if ("start".equals(cave))
                continue;
            
            if (Character.isLowerCase(cave.charAt(0)) && path.contains(cave))
                continue;

            List<String> newPath = new ArrayList<>(path);
            newPath.add(cave);
            solutions += recursePath(connecties, newPath);            
        }
        
        return solutions;
    }

    @Override
    public Integer solver1(Map<String, List<String>> input) {
        //return findPaths(input, false);
        return recursePath(input, new ArrayList<>() {{ add("start");}});
    }
    
    @Override
    public Integer solver2(Map<String, List<String>> input) {
        return findPaths(input, true);
    }

}
