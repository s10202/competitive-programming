package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;
    
public class Day24Test {
    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(24);

    Day24 day;
    
    @Before
    public void before() {
        day = new Day24();
    }

    @Test
    public void part1() {
        assertEquals(36969794979199L, (long)day.part1(input.getLines()));
    }
    
    @Test
    public void part2() {
        assertEquals(11419161313147L, (long)day.part2(input.getLines()));
    }
}
