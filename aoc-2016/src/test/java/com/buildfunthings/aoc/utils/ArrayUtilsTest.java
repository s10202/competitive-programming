package com.buildfunthings.aoc.utils;

import java.util.Arrays;

import org.junit.Test;

public class ArrayUtilsTest {
    @Test
    public void iCanRotateArrays() {
        int[] a = new int[] { 1, 2, 3 };

        ArrayUtils.leftRotate(a, 1, a.length);
        System.out.println(Arrays.toString(a));
        ArrayUtils.rightRotate(a, 1, a.length);
        System.out.println(Arrays.toString(a));
        ArrayUtils.rightRotate(a, 5, a.length);
        System.out.println(Arrays.toString(a));
    }
            
}
