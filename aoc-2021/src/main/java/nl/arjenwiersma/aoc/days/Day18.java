package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.List;

import nl.arjenwiersma.aoc.common.NewDay;

public class Day18 extends NewDay<List<Snailfish>, Long> {
    
    @Override
    public List<Snailfish> parseInput(List<String> input) {
        List<Snailfish> sf = new ArrayList<>();
        for (String in : input) {
            Snailfish sn = Snailfish.parse(in);
            sf.add(sn);
            Snailfish sfo = Snailfish.parse(in);
            assert (sn.toString().equals(sfo.toString()));
        }
        
        return sf;
    }

    @Override
    public Long solver1(List<Snailfish> input) {
        Snailfish s1 = input.get(0);
        for (int i = 1; i < input.size(); i++) {
            s1 = s1.add(input.get(i));
        }
        return s1.magnitude();
    }

    @Override
    public Long solver2(List<Snailfish> input) {
        long max = 0L;

        for (Snailfish sf1 : input) {
            for (Snailfish sf2 : input) {
                if (sf1.equals(sf2))
                    continue;
                
                Snailfish p1 = Snailfish.add(sf1.clone(), sf2.clone());
                
                Snailfish p2 = Snailfish.add(sf2.clone(), sf1.clone());

                max = Math.max(max, p1.magnitude());
                max = Math.max(max, p2.magnitude());
            }
        }
        
        return max;
    }

    
}
