package nl.arjenwiersma.aoc.days;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.arjenwiersma.aoc.common.DayInputExternalResource;

public class Day07Test {

    @Rule
    public DayInputExternalResource input = new DayInputExternalResource(07);

    Day07 day;

    @Before
    public void before() {
        day = new Day07();
    }

    @Test
    public void testSample() {
        String in = """
                $ cd /
                $ ls
                dir a
                14848514 b.txt
                8504156 c.dat
                dir d
                $ cd a
                $ ls
                dir e
                29116 f
                2557 g
                62596 h.lst
                $ cd e
                $ ls
                584 i
                $ cd ..
                $ cd ..
                $ cd d
                $ ls
                4060174 j
                8033020 d.log
                5626152 d.ext
                7214296 k""";

        assertEquals(95437, (long) day.part1(Arrays.stream(in.split("\n")).toList()));
        assertEquals(24933642, (long) day.part2(Arrays.stream(in.split("\n")).toList()));
    }

    @Test
    public void part1() {
        assertEquals(1611443, (long) day.part1(input.getLines()));
    }

    @Test
    public void part2() {
        assertEquals(2086088, (long) day.part2(input.getLines()));
    }
}
