package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.List;

import nl.arjenwiersma.aoc.common.NewDay;

public class Day06 extends NewDay<int[], Integer> {

    @Override
    public int[] parseInput(List<String> input) {
        return input.get(0).chars().toArray();
    }

    private int markerStart(int[] input, int size) {
        for (int i = 0; i < input.length - size; i++) {

            List<Integer> vals = new ArrayList<>();
            for (int x = 0; x < size; x++) {
                if (!vals.contains(input[i + x])) {
                    vals.add(input[i + x]);
                }
                if (vals.size() == size) {
                    return i + x + 1;
                }
            }
        }
        return -1;
    }

    @Override
    public Integer solver1(int[] input) {
        return markerStart(input, 4);
    }

    @Override
    public Integer solver2(int[] input) {
        return markerStart(input, 14);
    }

}
