package nl.arjenwiersma.aoc.days;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import nl.arjenwiersma.aoc.common.Day;

public class Day15 implements Day<Long> {
    @Override
    public Long part1(List<String> input) {
        return safestPathRisk(input, 1);
    }

    @Override
    public Long part2(List<String> input) {
        return safestPathRisk(input, 5);
    }

    private long safestPathRisk(List<String> input, int reps) {
        int height = input.size();
        int width = input.get(0).length();
        int[][] grid = new int[height][width];

        for (int i = 0; i < height; i++) {
            grid[i] = Arrays.stream(input.get(i).split("")).mapToInt(Integer::parseInt).toArray();
        }

        // Use Dijkstra's algorithm to determine the most optimal risk per cell

        // Board to hold risk values as provided by Dijkstra's algorithm
        int[][] board = new int[height * reps][width * reps];
        dijkstra(grid, board);
        return (long) board[board.length - 1][board[0].length - 1];
    }

    /**
     * height and width of actual grid required
     */
    private int valueInGrid(int[][] grid, int x, int y, int width, int height) {
        int v = (grid[y % height][x % width] - 1 + x / width + y / height) % 9 + 1;
        return v;
    }

    private void dijkstra(int[][] grid, int[][] board) {
        int height = board.length;
        int width = board[0].length;

        int gH = grid.length;
        int gW = grid[0].length;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                board[y][x] = Integer.MAX_VALUE;
            }
        }
        board[0][0] = 0;

        PriorityQueue<int[]> q = new PriorityQueue<>(Comparator.comparingInt(x -> x[2]));
        q.add(new int[] { 0, 0, 0 }); // start at 0,0 with 0 risk

        while (!q.isEmpty()) {
            int[] c = q.poll();

            int y = c[0];
            int x = c[1];

            if (c[2] != board[y][x])
                continue;

            for (int[] n : new int[][] { { -1, 0 }, { 1, 0 }, { 0, 1 }, { 0, -1 } }) {
                int nY = y + n[0];
                int nX = x + n[1];

                if (nY < 0 || nY >= height || nX < 0 || nX >= width)
                    continue;

                if (board[y][x] + valueInGrid(grid, nX, nY, gW, gH) < board[nY][nX]) {
                    board[nY][nX] = board[y][x] + valueInGrid(grid, nX, nY, gW, gH);
                    q.add(new int[] { nY, nX, board[nY][nX] });
                }
            }
        }
    }

}
