package nl.arjenwiersma.aoc.days;

import java.util.ArrayList;
import java.util.List;

import lombok.ToString;
import nl.arjenwiersma.aoc.common.Day;

public class Day16 implements Day<Long> {
    private int parse_n_number(String packet, int pos, int n) {
        return Integer.valueOf(packet.substring(pos, pos + n), 2);
    }
    
    private String parse_n_string(String packet, int pos, int n) {
        return packet.substring(pos, pos + n);
    }

    private String getBinaryString(Character c) {
        return String.format("%4s", Integer.toBinaryString(Integer.valueOf(String.valueOf(c), 16))).replace(' ', '0');
    }

    @ToString
    class Packet {
        int type;
        int version;
        List<Packet> children = new ArrayList<>();

        public Packet(int type, int version) {
            this.type = type;
            this.version = version;
        }

        public int sumVersions() {
            return version + children.stream().mapToInt(x -> x.sumVersions()).sum();
        }
    }

    class LiteralPacket extends Packet {
        long value;

        public LiteralPacket(int type, int version) {
            super(type, version);
        }

        @Override
        public String toString() {
            return  "LiteralPacket [value=" + value + "] " + super.toString() ;
        }

        
    }

    class OperationPacket extends Packet {
        public OperationPacket(int type, int version) {
            super(type, version);
        }

        private long packetValue(Packet x) {
            if (x instanceof LiteralPacket) {
                return ((LiteralPacket) x).value;
            } else {
                return ((OperationPacket) x).apply();
            }
        }

        public long apply() {
            switch (type) {
            case 0 -> {
                return children.stream().mapToLong(this::packetValue).reduce(0, (a, b) -> a + b);
            }
            case 1 -> {
                return children.stream().mapToLong(this::packetValue).reduce(1, (a, b) -> a * b);
            }
            case 2 -> {
                return children.stream().mapToLong(this::packetValue).min().getAsLong();
            }
            case 3 -> {
                return children.stream().mapToLong(this::packetValue).max().getAsLong();
            }
            case 5 -> {
                return (packetValue(children.get(0)) > packetValue(children.get(1))) ? 1 : 0;
            }
            case 6 -> {
                return (packetValue(children.get(0)) < packetValue(children.get(1))) ? 1 : 0;
            }
            case 7 -> {
                return (packetValue(children.get(0)) == packetValue(children.get(1))) ? 1 : 0;
            }
            }
            return 0;
        }

        @Override
        public String toString() {
            return "OperationPacket [] " + super.toString();
        }

    }
    
    public int parsePacket(Packet parent, String input, int pos) {
        int version = parse_n_number(input, pos, 3);
        int type = parse_n_number(input, pos + 3, 3);
        pos += 6;

        switch (type) {
        case 4 -> {
            LiteralPacket lp = new LiteralPacket(type, version);
            
            boolean more = true;
            StringBuilder sb = new StringBuilder();
            while (more) {
                more = parse_n_number(input, pos, 1) == 1 ? true : false;
                sb.append(parse_n_string(input, pos + 1, 4));
                pos += 5;
            }
            lp.value = Long.parseLong(sb.toString(), 2);
            parent.children.add(lp);
        }
        default -> {
            OperationPacket op = new OperationPacket(type, version);

            int length = parse_n_number(input, pos, 1);
            int v = parse_n_number(input, pos + 1, length == 1 ? 11 : 15);
            pos += 1 + (length == 1 ? 11 : 15);
            
            if (length == 0) {
                int maxPos = pos + v;
                while (pos < (maxPos-6)) // min literal packet size
                    pos = parsePacket(op, input, pos);
            } else {
                for (int i = 0; i < v; i++) {
                    pos = parsePacket(op, input, pos);
                }
            }
            parent.children.add(op);
        }
        }
        return pos;
    }
    
    @Override
    public Long part1(List<String> input) {
        String program = input.get(0);

        List<Character> chars = program.chars().mapToObj(x -> Character.valueOf((char) x)).toList();

        StringBuilder sb = new StringBuilder();
        for (Character c : chars) {
            sb.append(getBinaryString(c));
        }
        String converted = sb.toString();

        Packet p = new Packet(0,0);
        parsePacket(p, converted, 0);

        return (long) p.sumVersions();
    }

    @Override
    public Long part2(List<String> input) {
        String program = input.get(0);
        List<Character> chars = program.chars().mapToObj(x -> Character.valueOf((char) x)).toList();

        StringBuilder sb = new StringBuilder();
        for (Character c : chars) {
            sb.append(getBinaryString(c));
        }

        String converted = sb.toString();

        Packet p = new Packet(0,0);
        parsePacket(p, converted, 0);

        for (Packet c : p.children) {
            if (c instanceof OperationPacket) {
                return ((OperationPacket) c).apply();
            }
        }
        
        return 0L;
    }

}
